#### Hướng dẫn test ở thị trường SEA khi có thông báo “Sorry, this service does not support for your country:VN!”

March 27, 2020 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/)

B1: Vào **Appstore/CH Play** tùy thuộc vào thiết bị hiện có tìm và tải app

![](media/6621f14b060a39731dfb2d543f2b60e0.png)

B2: **Dùng** Wifi Guest \> Truy cập <https://www.tcpvpn.com/> \> Tìm VPN ở quốc
gia muốn test \> Ở đây mình sẽ chọn ThaiLand do đang có nhu cầu test

B3: Lựa chọn 1 trong các gói hiện có \> Chọn **Create Username and Password** để
tạo account

B4: **Tạo account** và **Download Premium VPN Config (.ovpn)**

![](media/83496cac3931e6fef9ab6dd76fef391e.png)

B5: Giải nén file vừa download ở B4

B6: Dùng app OVPN đã tải ở B1 \> **import file** ở B5 \> Nhập thông tin
**username + password** ở B4 \> **Save** lại

![](media/8f4081766512c6448841d9ba1876e65c.png)

![](media/727eb46e88e2e2cd2f2974fb6be2d85d.png)

B7: Enable để sử dụng

![](media/ea06fac0266d1cf76e7bbaecf28f5fca.png)

B8: Đóng tất cả các trình duyệt hiện có và mở lại để tiến hành test
