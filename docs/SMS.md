# **Hướng dẫn Thanh toán bằng phương thức SMS ở môi trường Sandbox trên HĐH Windows**

\* Test trên Production: Dùng sim trả trước của cá nhân để thanh toán.

\* Test trên Sandbox: *Làm theo các bước dưới đây*

-   Bước 1.  Mở notepad bằng **Run as administrator**

-   Bước 2.  Chọn theo đường dẫn: **C:\\Windows\\System32\\drivers\\etc\\hosts**

-   Bước 3.  Addhost**: 10.30.8.220 sandbox.mresource.pay.zing.vn** -\> **Save**

![](media/24cb84989c22e4392c1cae88d731426d.png)

![](media/96021b94b3bc608da48f9570a4640369.png)

**Link test**

-   Viettel:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/viettel-charging-gateway/>

-   Vinaphone:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/vnpt-charging-gateway/>

**Flow test:**

*1.  Cú pháp 1: DK*

-   Bước 1: Copy cú pháp  
    

   ![](media/148d3d04b8e1d8a18aebc8014fcc8268.png)

      
-   Bước 2: Paste cú pháp, chọn config theo hình. Thay đổi syntax (dán cú pháp), amount (giá tiền phù hợp), Msisdn (nhập tùy ý), Mode (chọn real).

>   **Viettel**   
>    

![](media/ecd2cfa9f3f870f4ca1426c00b0c7765.png)

>   **Vinaphone**  
>   

![](media/bc269a3ef1598fda74fe8c3314f84ce0.png)

     
-   Bước 3: Chọn **SubmitOrder** -\> “Result: 0\|Thanh cong (VNG DK10 KTM 20190710000005822)”  
-   Bước 4: Kiểm tra kết quả giao dịch ở trang payment

*2.  Cú pháp 2: NAP*  

-   Bước 1: Copy cú pháp  
    

    ![](media/04cc04e913c2bd4bf6e5663621b8533c.png)

      
-   Bước 2: Paste cú pháp, chọn config theo hình. Thay đổi syntax (dán cú pháp), amount (giá tiền phù hợp), Msisdn (nhập tùy ý), Mode (chọn real).
    

    ![](media/1770b11ceb7b5abd2d22733c06a372b8.png)

      
-   Bước 3: Chọn **SubmitOrder** -\>”Result: 0\|Ban da nap thanh cong 10000 VND vao game Gun
    Pow. Chi tiet LH https://www.facebook.com/gunpow.360game.vn/”  
-   Bước 4: Kiểm tra kết quả giao dịch ở trang payment
