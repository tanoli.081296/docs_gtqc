# **Hướng dẫn thanh toán trên WebShop bằng ZingCard ở môi trường Sandbox**

\* Test trên Sandbox: Xem mục 1.1 dưới dây để lấy thẻ Sandbox.

\* Test trên Production: Mua trực tiếp trên OA Zalo: *Esale Hệ thống bán lẻ trực tuyến* hoặc <https://esale.zing.vn/>

1.1. Cách lấy thẻ Zing Card trên Sandbox:

-   Bước 1: Truy cập link: <https://sandbox.mresource.pay.zing.vn/card/zing>

-   Bước 2: Login bằng domain VNG.

-   Bước 3: Chọn mệnh giá thẻ cần generate.

-   Bước 4: Sử dụng Serial/PIN code được cung cấp để thanh toán trên Sandbox.

\*\*\*Nếu không vào được link gen card thì liên hệ domain anh Chiêu: **ChieuVH** để được giúp đỡ.

**Hướng dẫn test nạp thẻ zing card trên sandbox**

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán ZingCard

-   Bước 3: Điền thông tin thẻ (serial, mã nạp) đã được generate từ bước trên

-   Bước 4: Click button “Xác nhận” để nạp tiền

![](media/babf82b1d956cc412d1b7efb40161aec.png)

Bước 5a: Nếu thẻ hợp lệ: Sẽ trả ra trang kết quả giao dịch Thanh toán
thành công (users được cộng items)

![](media/2599536e943e99c9d8a78f97282dfb98.png)

Bước 5b: Nếu thẻ không hợp lệ: Sẽ hiển thị ngay thông báo lỗi ở ô Nạp thẻ
cào (users không được cộng items)

![](media/4e3a1b4b1127aab4fa2e533050193a79.png)

# *Nếu game có bật filter mệnh giá thẻ theo giá gói khi nhập số seri sẽ hiển thị lỗi*

![](media/huyvh2.png)