#### Hướng dẫn add host để phục vụ việc test Sandbox/Staging(Addhost) trên PC

May 14, 2019 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/), [Project
Requirement_QC](https://wiki.mto.zing.vn/category/mto-billing/project-requirement_qc/)

### **I. Windows**

### 1. Vào thanh tìm kiếm search Notepad/WordPad \> **RightClick** \> Chọn **Run as Administrator**

![](media/a629d7c94a65986d0b9d85f4f12a590a.png)

### 2. Tìm đến đường dẫn **C:\\Windows\\System32\\drivers\\etc \>** Chọn **All Files \>** Chọn **hosts**

![](media/bc15f21f4f54739610d8bd332a2d0bbc.png)

### 3. Thêm đoạn này và Lưu lại

#### **122.201.11.103 stc-billing.mto.zing.vn billing.mto.zing.vn pay.zing.vn pay.mto.zing.vn sandbox-pay.mto.zing.vn**

### 4. Truy cập <https://whatismyipaddress.com/> để lấy IPv4

![](media/b7a3148798ac9284c7cc5ee3ef22e695.png)

### 5. Gửi IPv4 cho trongln\@vng.com.vn hoặc datlc\@vng.com.vn để add vào whitelist và có thể tiến hành test

\*\*\*Lưu ý: Nên tổng hợp lại list nhiều IPv4 để add 1 lượt, tránh riêng lẻ từng cái mất thời gian.
---------------------------------------------------------------------------------------------------
