# HƯỚNG DẪN KIỂM TRA VÀ NGHIỆM THU WEB SHOP SANDBOX VIỆT NAM

***WebShop là nơi bán vật phẩm trong game trên nền tảng Web của VNG.***

***WebShop được phát triển nhằm mục đích phục vụ user ở từng quốc gia( local-user) thanh toán bằng các kênh thanh toán phù hợp và phổ biến ở quốc gia của mình(local-payment).*** 


![](test/sbvn.drawio)


Hình 1: Workflow in WebShop

## **I. HƯỚNG DẪN TEST PHƯƠNG THỨC ĐĂNG NHẬP**

-  Bước 1: Chọn sản phẩm cần test trên Sandbox (ex:<https://sandbox-pay.mto.zing.vn/wplogin/mobile/gamecode>)

    ### **ĐỊNH DANH TÀI KHOẢN BẰNG SOCIAL ACCOUNT**

-  Bước 2: Click vào icon tương ứng để chọn các dạng định danh tài khoản *(Zalo/Facebook/TK Chơi ngay/Google/ZingID)* và đảm bảo UI/UX, banner trên cả PC/laptop và mobile đúng và đủ theo yêu cầu


![](media/snk123.png)

- Bước 3: Sau khi login cần kiểm tra account có trả về đúng **Cụm máy chủ**, **server**, **role**, **level**, **v.v...** Xác nhận đúng tài khoản trước khi tiến hành ở bước tiếp theo


    ### **ĐỊNH DANH TÀI KHOẢN BẰNG ID NHÂN VẬT**  
  
**Định danh tài khoản thông qua server**

  Click vào icon *ID Nhân vật* sẽ redirect sang trang đăng nhập và tiến hành đăng nhập


![](media/login2.png)

![](media/login2a.png)

**Định danh tài khoản tại Trang đăng nhập**

  Click vào icon *ID Nhân vật* và nhập đúng ID nhân vật theo hướng dẫn

![](media/snk1.png)

![](media/snk2.png)


<p style="color:red"><b>LƯU Ý: Để đảm bảo Point tồn được sử dụng đúng theo mong muốn của chủ tài khoản. Khi dùng ID NHÂN VẬT để tiến hành thanh toán ở WEBSHOP. Có thể bật tính năng không được phép sử dụng point tồn để thanh toán Tùy THUỘC VÀO cơ chế của GAME và hiện Lưu Ý trong ô đăng nhập</b></p>


![](media/jxm.png)


## **II. HƯỚNG DẪN TEST CẤU HÌNH GÓI VẬT PHẨM**

![](media/webshop1.png)


-  <p style="color:red">Kiểm tra về UI hiển thị trên các thiết bị PC/Laptop/Mobile</p>
-  **Đối với gói vật phẩm có điều kiện phải hiện thị đúng với yêu cầu in-game (gói chỉ bán trên server nào, yêu cầu level nhân vật đạt cấp bao nhiêu,etc,...)**

![](media/hinhanh.png)

-  <p style="color:red">Kiểm tra phần mô tả gói</p>

![](media/mota.png)

-  <p style="color:red">Kiểm tra gói vật phần có đúng tab so với yêu cầu</p>

![](media/tab.png)

-  <p style="color:red">Kiểm tra mua đúng gói vật phẩm cho <b>Nhân vật</b> và <b>Server</b></p>

![](media/thongtin.png)

# *Giao diện trên mobile*

![](media/ssvnm.png)

## **III. TIẾN HÀNH THANH TOÁN**

###*1. Với Game Studio sử dụng gói vật phẩm:*

-   Bước 1: Chọn gói vật phẩm cần mua trong danh sách hiện có

![](media/huyvh1.png)

-   Bước 2: Xác nhận "Chọn gói nạp này"

**Nếu giá trị Point lớn hơn hoặc bằng giá trị gói thì sẽ ưu tiên đổi vật phẩm bằng Point**

-   Bước 3: Chọn phương thức thanh toán tương ứng sẽ hiện ra với mỗi gói

	| Phương thức thanh toán 	| Mệnh giá thanh toán (VNĐ)              	                                 |
	|------------------------	|----------------------------------------------------------------------------|
	| ZingCard               	| 10K, 20K, 50K, 100K, 200K, 500K, 1000K 	                                 |
	| ATM/iBanking           	| Từ 20K trở lên                    	                                     |
	| Thẻ tín dụng           	| Từ 20K trở lên                    	                                     |
	| Zalopay                	| Từ 5K trở lên                         	                                 |
	| SMS VNPT               	| 10K, 20K, 50K, 100K, 200K                                                	 |
	| SMS Viettel            	| 10K, 20K, 50K, 100K, 200K, 500K (chỉ áp dụng cho game (Tỉ Muội Hoàng Cung) |

    -   Lưu ý mệnh giá tham khảo quy đổi SMS (Áp dụng theo chính sách tỉ giá VNG theo từng game)

        | Giá tiền | 10K     | 20K      | 50K      | 100K     | 200K      |
        |----------|---------|----------|----------|----------|-----------|
        | Point    | 8 point | 16 point | 40 point | 80 point | 160 point |

    -	Phương thức SMS chỉ hỗ trợ thanh toán tối đa gói giá trị 200.000 VNĐ (Tùy thuộc vào chính sách VNG theo từng game)

###*2. Với Game Studio không sử dụng gói vật phẩm:*
-   Hỗ trợ cả 5 phương thức thanh toán: 
      - Thanh toán bằng ZaloPay 
      - Thanh toán bằng ZingCard
      - Thanh toán bằng ATM/iBanking 
      - Thanh toán bằng thẻ tín dụng
      - Thanh toán bằng SMS

# **Hướng dẫn test các phương thức thanh toán trên môi trường Sandbox tại thị trường Việt Nam**    

*2.1 [Thanh toán Zing Card](../Zingcard)*

*2.2 [Thanh toán SMS](../SMS)*

*2.3 [Thanh toán Zalopay](../Zalo Pay)*

*2.4 [Thanh toán Thẻ tín dụng ](../Thẻ tín dụng)*

*2.5 Thanh toán ATM/iBanking*

    \* Test trên Sandbox: **chỉ test được flow tới bước hiển thị form nhập thông
tin.**

    \* Test trên Production: Dùng tài khoản hoặc thẻ ATM có iBanking để thanh toán

