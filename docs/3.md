#### [SEA][Myanmar] Hướng dẫn thanh toán MTO Billing ở thị trường SEA-Myanmar

July 18, 2018 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/)

Myamar sử dụng Kyat Myanmar (MMK)

Giai đoạn 1:

Ở Myanmar, ROS moible sẽ tích hợp:

-   Coda (ưu tiên làm trước) mở hết kênh của Coda:

    -   SMS: MPT, Telenor, Ooredoo

    -   E-wallet: Wave money

    -   Red dot pay pin (1 dạng epin)

Giai đoạn 2:

-   Easypoint (1 dạng epin): đã có sẵn hợp đồng, GBD cung cấp thông tin tích hợp
    cho MTO sau

-   MPSS (cung cấp các e-wallet True, OK\$, M-Piatsan): chờ confirm của GBD

Hướng dẫn mọi người cách tiến hành login và thanh toán MTO Billing ở thị trường
SEA-Thailand (Trong bài viết là ví dụ với game Rules Of Survival)

**1. Thanh toán qua Ví Tồn (MTO Wallet) của Web Payment**

\* Nếu tiền tồn lại trong ví của user đủ để mua gói đang chọn (ví dụ ví tồn của
user đang có 925 MMK, và chọn mua gói 30 MMK) thì chỉ WP sẽ hiện phương thức
thanh toán là **Ví Tồn (MTO Wallet)** để user có thể sử dụng tiền tồn trong ví
thanh toán, thay vì tiến hành các phương thức thanh toán khác.

![](media/12f61ebb4e325108ad842b4283ca0fac.png)

Step 1: Proceed để ví tồn còn tiền

Step 2: Mua gói nhỏ hơn ví tồn còn lại

Step 3: WP điều hướng đến lựa chọn Account Balance.

Step 4: Click Confirm để xác nhận thanh toán

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/586dc061bc68d6702ac8cc44c341f0b9.png)

\* Nếu tiền tồn tại trong ví của user thấp hơn gói đang lựa chọn (ví dụ còn 6THB
nhưng chọn mua gói 30THB), thì sẽ có 1 pop-up thông báo để user click
“Confirmed” và tiếp tục thanh toán với các phương thức thanh toán dưới đây:

![](media/564ed1de039104a5701fa28f1c3d4f66.png)

**2. Thanh toán qua SMS**

-   Hiện có 1 nhà cung cấp (Providers) cho dịch vụ thanh toán qua SMS là CODA.
    Cần xác định xem các phương thức SMS đang sử dụng là thuộc Providers nào
    (nếu có bổ sung) để nhập thông tin thẻ chính xác.

Mẫu số điện thoại tham khảo:

-   MPT: 9961350000, 09690000966, 17012321, 09426000323, (+959690000966)

-   Telenor: 09790097900 (+*95*9790097900)

-   Ooredoo: 09970000234 (+959970000234)

Thanh toán qua SMS của Provider – CODA

\*Mức giá hỗ trợ tham khảo CODA:

| Partner | Channel | Country | Provider | Transactions expire in | Giới hạn \$ | Mệnh giá hỗ trợ (MMK)        |
|---------|---------|---------|----------|------------------------|-------------|------------------------------|
| CODA    | SMS     | Myanmar | MPT      |                        |             | 1k, 3k, 5k, 10k, 30k,…       |
| CODA    | SMS     | Myanmar | Ooredoo  |                        |             | 1k, 3k, 5k, 10k, 30k, 50k, … |
| CODA    | SMS     | Myanmar | Telenor  |                        |             | 1k, 3k, 5k, 10k, 30k, 50k, … |

Step 1: Vào trang thanh toán WP RosThai (ex:
https://sandbox-billing.mto.zing.vn/webshop/rosmym/\#/pay)

Step 2: Chọn gói và phương thức thanh toán là SMS

Step 3: Chọn 1 t

![](media/acd29e53dfb82884bb05b0aa72d434e1.png)

rong 3 nhà mạng MPT/Ooredoo/Telenor, sau đó click Confirmed.

Step 4a: Với Ooredoo hay Telenor, WP sẽ điều hướng ra trang result, với message
hướng dẫn users soạn tin nhắn từ devices đến số của tổng đài để thanh toán (có
kèm thông tin Payment ID)

![](media/6791561d8e6f72849bce6dfb25168170.png)

![](media/42efd50c13b796a9282d0ffdadec8bf7.png)

Step 4a-1: Trên SANDBOX environment, với Ooredoo và Telenor để vào link
(https://stage.codapayments.com/epcsimulator/sendConfirmSms.action) và làm theo
hướng dẫn sau

![](media/5ff9c98fd86dd2a7fb1c132ca7bcdd54.png)

![](media/01ddd565073cc19993a2f065cb757afc.png)

Step 4a-1: Trên Staging/Production environment, vui lòng nhờ đối tác bên Myanmar
nhắn tin hộ để xác thực giao dịch,

Step 4b: Với MPT, WP sẽ điều hương sang trang thanh toán CodaPayments.com, điền
số điện thoại, sau đó nhập mã thanh toán 3-digit nhận được trên thiết bị để tiến
hành thanh toán

-   Với SANDBOX environment thì điền 777 hoặc 7777 để giao dịch thành công)

-   Với Staging/Production environment, vui lòng nhờ đối tác bên Myanmar nhắn
    tin hộ để xác thực giao dịch,

![](media/efb6908bd7f10e5b9e44b0d1648afa79.png)

![](media/56870126c043ad74a31c2559bd2fc86f.png)

![](media/c9c28221023e438be0a668116fc6ad26.png)

Step 5: Trên devices, mở trình soạn thảo văn bản (tin nhắn sms) với nội dung như
hướng dẫn ở trang result gửi đến tổng đài

Step 6: Sau khi thanh toán thành công, quay trở lại WP. Click “Check the payment
results”, WP sẽ refresh và trả về kết quả giao dịch, users được nhận items, tiền
dư sẽ vào ví tồn.

![](media/711d5ed3c602c7b5d50034bf698a017a.png)

**3. Thanh toán qua Cashcard**

-   Hiện có 2 nhà cung cấp (Providers) cho dịch vụ thanh toán Cashcard là CODA
    và 2c2p. Cần xác định xem các phương thức Cashcard đang sử dụng là thuộc
    Providers nào để nhập thông tin thẻ chính xác.

\* Mệnh giá hỗ trợ tham khảo

| Partner | Channel  | Country | Provider        | Giới hạn \$ mỗi Tnx | Giới hạn \$ | Mệnh giá hỗ trợ (MMK)                                         |
|---------|----------|---------|-----------------|---------------------|-------------|---------------------------------------------------------------|
| CODA    | CashCard | Myanmar | Red dot pay Pin |                     | unlimited   | 500, 1k, 1k5, 2k, 3k, 5k, 8k, 10k, 15k, 20k, 30k, 50k, 100k,… |
| 2c2p    | CashCard | Myanmar | Easy Point      |                     |             |                                                               |
|         |          |         |                 |                     |             |                                                               |

\*Các cách thức mua thẻ nạp Cashcard (Red dot pay Pin)để thanh toán thật trên
Production/Staging:

-   Mua thẻ ở các cửa hàng Red Dot store in Myanmar (For more Details –
    <http://bit.ly/2id3r4s>), no way to buy online.

**3a. Thanh toán qua Cashcard CODA (Red dot pay Pin)**

Step 1: Chọn phương thức thanh toán là Cashcard

Step 2: Chọn thẻ của nhà cung cấp (Red dot pay Pin) và nhập số điện thoại

![](media/c868b43fe11ab10971ebb9ff218702eb.png)

Step 3: WP sẽ điều hướng sang trang CodaPayments.com để nhập mã PIN của thẻ (mã
nạp tiền, 15-digit)

-   Trên Sandbox environment: Xin list thẻ ảo Sandbox từ đối tác Coda – VNG

-   Trên Production environment: Nhờ đối tác CODA – VNG mua hộ vài thẻ thật rồi
    thanh toán.

![](media/95dbb2e38c601f11fbbfb72a909e7756.png)

Step 4: Nhập mã thành công sẽ nhận được kết quả thông báo như sau, click “Close”
để kết thức giao dịch.

![](media/2b534df6e3b19a09c1186d9c72a676a5.png)

Step 4: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/e227e3b61b8e29eeba0fa5d269856345.png)

**3a. Thanh toán qua Cashcard 2c2p (Easy Point)**

Step 1: Chọn phương thức thanh toán là Cashcard

Step 2: Chọn thẻ của nhà cung cấp (Easy Point)

Step 3: Điên thông tin Card Serial/Pin code như yêu cầu

-   Trên Sandbox environment: Xin list thẻ ảo Sandbox từ đối tác Easypoint MM

-   Trên Production environment: Nhờ đối tác Easypoint MM mua hộ vài thẻ thật
    rồi thanh toán.

![](media/bd1c0176a2b7c1064032467cf77c6f76.png)

**4. Thanh toán qua E-Wallets “Wave Money”**

\* Để mua ở Staging/Production có thể liên hệ Fajri Gustia Nanda trong group
CODA – VNG để mượn tài khoản hoặc nhờ test hộ. Tutorial Video có thể tham khảo ở
.

\*Mệnh giá hỗ trợ tham khảo:

| Partner | Channel  | Country | Provider   | Giới hạn \$ mỗi Tnx | Giới hạn \$ | Mệnh giá hỗ trợ (MMK)          |
|---------|----------|---------|------------|---------------------|-------------|--------------------------------|
| CODA    | E-Wallet | Myanmar | Wave Money |                     |             | 1k, 5k, 10k, 50k, 100k, 200k,… |

Step 1: Chọn gói với phương thức thanh toán là E-Wallet/Wave Money

Step 2: Điền số điện thoại, sau đó Click “Confirm”

![](media/6b403ab1c818c36a0e5e5d749ba3df3b.png)

Step 3: WP điều hướng sang trang thanh toán CodaPayments – Wave Money, tại đây
điền số điện thoại

![](media/5eb660655269d79eb3f043ff0b717425.png)

Step 4: Nhập sốđiện thoại, sau đó Login với tài khoản Wave Money và tiến hành
thanh toán

![](media/27944ef7914fbf746da367dc6069958d.png)

Step 5: Trên Sandbox/Staging/Production: Nhờ đối tác sử dụng thiết bị đã cài app
Wave Money và có sẵn account, nhập số PIN để tiến hành thanh toán.
