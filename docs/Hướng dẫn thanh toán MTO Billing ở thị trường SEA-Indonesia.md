#### [SEA][Indonesia] Hướng dẫn thanh toán MTO Billing ở thị trường SEA-Indonesia

April 17, 2018 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/)

Hướng dẫn mọi người cách tiến hành login và thanh toán MTO Billing ở thị trường
SEA-Indonesia (Trong bài viết là ví dụ với game Rules Of Survival)

**I. Login**

1. Option 1 – Login with character name:

![](media/21285dd4f80205e3018ea4090cf30750.png)

Step 1a: Chọn server

Step 1b: Điền username của người chơi, lưu ý điền đúng chữ cái in hoa, in thường
(tài khoản Facebook sẽ có id khác tài khoản facebook, fACEbook, etc…)

2. Option 2 – Login with RoleID:

\*RoleID không phải ID trong game, mà là RoleID của WP, có thể thấy RoleID sau
khi login with character name.

![](media/92394e7728bda9ee96c95d869daf740c.png)

Step 2a: Chọn server

Step 2b: Điền RoleID

**II. Payment Processing**

**1. Thanh toán qua Ví Tồn (MTO Wallet) của Web Payment**

\* Nếu tiền tồn lại trong ví của user đủ để mua gói đang chọn (ví dụ ví tồn của
user đang có 20.000 IDR, và chọn mua gói 15.000 IDR) thì chỉ WP sẽ hiện phương
thức thanh toán là **Ví Tồn (MTO Wallet)** để user có thể sử dụng tiền tồn trong
ví thanh toán, thay vì tiến hành các phương thức thanh toán khác.

Step 1: Proceed để ví tồn còn tiền (ví dụ có 25.000 IDR)

Step 2: Mua gói nhỏ hơn ví tồn còn lại (ví dụ mua gói 15.000 IDR)

![](media/fcd4bc475b43dd2dee92ada69935f33f.png)

Step 3: WP điều hướng đến lựa chọn Account Balance.

Step 4: Click “Lanjut/Confirm” để xác nhận thanh toán

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/b38fc3dc7f3aef9112ebf429e2689e82.png)

\* Nếu tiền tồn tại trong ví của user thấp hơn gói đang lựa chọn, thì sẽ có 1
pop-up thông báo để user click “Lanjut/Confirm” à tiếp tục thanh toán với các
phương thức thanh toán dưới đây:

![](media/0d97eda2fe7e909a199550fafc96c07f.png)

**2. Thanh toán qua SMS**

-   Hiện có 2 nhà cung cấp (Providers) cho dịch vụ thanh toán qua SMS là Coda và
    Metranet. Cần xác định xem các phương thức SMS đang sử dụng là thuộc
    Providers nào để nhập thông tin chính xác.

\* Các Payment Partner và mức giá hỗ trợ tham khảo:

| Partner  | Channel | Country | Provider        | Giới hạn \$ mỗi giao dịch | Giới hạn \$       | Mệnh giá hỗ trợ (1000IDR)                                                                                         |
|----------|---------|---------|-----------------|---------------------------|-------------------|-------------------------------------------------------------------------------------------------------------------|
| Coda     | dcb     | Indo    | indosat         | 1.000-\>1.100.000IDR      | 3.000.000 IDR/day | [Indosat_pricepoints](https://framework.mto.zing.vn/wiki/wp-content/uploads/2018/04/Indosat_pricepoints.csv)      |
| Coda     | dcb     | Indo    | XL              | 1.000-\>1.000.000IDR      | unlimited         | any in acceptable range                                                                                           |
| Coda     | dcb     | Indo    | Tri indonesia 3 | 500-\>1.300.000 IDR       | unlimited         | any in acceptable range                                                                                           |
| Coda     | dcb     | Indo    | Telkomsel       | 500-\>500.000IDR          | unlimited         | [Telkomsel_pricepoints](https://framework.mto.zing.vn/wiki/wp-content/uploads/2018/04/Telkomsel_pricepoints.xlsx) |
| Metranet | dcb     | Indo    | Telkomsel       |                           |                   |                                                                                                                   |

\* Trang web để hỗ trợ check tin nhắn trả về:
https://sms-online.co/receive-free-sms/6285574670577

Step 1: Chọn 1 trong 4 nhà cung cấp viễn thông (Network/Telecomunication
Provider)

![](media/e3e564dcc426c22708ab3af7385635f0.png)

Step 2: Điền đầu số điện thoại tương ứng với mỗi nhà mạng Indonesia, ví dụ:

XL (Coda): 087887705753, 081994141387- Fajri’s number (team CODA)

Tri 3 (Coda): 089507209582, 089643927109- Fajri’s number (team CODA)

Indosat (Coda):

-   085574670577
    ([linktoreadSMSIndosat577](https://sms-online.co/receive-free-sms/)),

-   085814727310,

-   085574719449
    ([linktoreadSMSIndosat](https://www.receivesms.co/indonesia-phone-number/814/)449)

-   085781117382- Fajri’s number (team CODA)

Telkomsel (Metranet): 081286533606, 08118051889 (team Metranet)

Step 3: Click “Lanjut/Continue” để tiếp tục.

Step 4a: Thanh toán qua Codapay partners (3 nhà mạng Indosat, XL, Tri 3) sẽ nhận
message trả về tại trang kết quả giao dịch với định dạng tham khảo như sau:

![](media/022f9128bae0ca878dd6e7b98c3be177.png)

(We sent an SMS to 85574670577. To complete the transaction, send a three-digit
code from SMS to 99288.)

Step 4b: Thanh toán qua Metranet (nhà mạng Telkomsei) sẽ nhận message trả về tại
trang kết quả giao dịch với định dạng tham khảo như sau:

– Terima kasih. Untuk melanjutkan pembelian, silakan cek SMS pada handphone Anda
081286533606 dan ikuti petunjuk selanjutnya

(thanks. To continue purchasing please check SMS on your mobile 081286533606 and
follow the next instruction)

Step 5: Đồng thời 1 tin nhắn từ tổng đài sẽ được gửi đến số điện thoại đã điền ở
trên với cú pháp hướng dẫn thanh toán. Khi users reply lại tin nhắn từ tổng đài
là đã xác nhận giao dịch, tin nhắn có định dạng tham khảo như sau:

-   Kirim “CODAPAY 917” ke 2000. Anda akan dikenakan biaya Rp. 142.353, termasuk
    PPN. (Send “CODAPAY 917” to 2000. You will be charged Rp. 142,353, including
    VAT.)

-   Kami sudah mengirimkan SMS ke 89507209582. Untuk menyelesaikan transaksi,
    kirim “BELI” diikuti 3 digit angka dari SMS ke 95333. Contoh: BELI 123. (We
    have sent the SMS to 89507209582. To complete the transaction, send “BUY”
    followed by 3 digit number from SMS to 95333. Example: BUY 123.)

-   Ketik BELI 880 sebagai balasan pesan ini untuk menyelesaikan pembelian.
    Rp180.000 akan dikurangi dari pulsa anda. CS:085779160178 (Type BUY 880 as a
    reply to this message to complete your purchase. Rp180.000 will be deducted
    from your credit. CS: 085779160178)

-   [Indosat]hati2 penipuan! abaikan sms ini jika tak lakukan transaksi. Balas
    BELI 377 utk selesaikan pembelian. Rp15.000 akan dikurangi dari pulsa anda.
    CS:081519193600

Step 6: Sau khi reply lại tin nhắn tổng đài với cú pháp như hướng dẫn, quay trở
lại WP, click “Check the payment results”.

-   Ex: [Indosat] Beli 377

Step 7: Sẽ có thêm 1 tin nhắn nữa thông báo từ tổng đài giao dịch đã thành công

-   Ex: [Indosat] Pembelian Anda di VNG-RoS Mobi sudah selesai. Rp15.000 sudah
    dikurangi dari sisa pulse Anda. Terima kasih atas pembelian anda.
    CS:081519193600

Step 8 : WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/b38fc3dc7f3aef9112ebf429e2689e82.png)

**3. Thanh toán qua Mini market/Convenient Store. (Payment Partner: Coda,
Channel: Otc)**

\*Indonesia có 3 thương hiệu chính là Alfamart, Indomaret và 7-Eleven, tuy nhiên
hiện nay WP chỉ làm việc với 2 thương hiệu đầu tiên là Alfamart và Indomaret.

-   Alfamart

-   Indomaret

-   7 – Eleven (hiện chưa hỗ trợ)

\* Mức hỗ trợ giá tham khảo:

| Partner | Channel | Country | Network Provider | Transactions expire in | Giới hạn \$       | Mệnh giá hỗ trợ (đơn vị: 1000IDR) |
|---------|---------|---------|------------------|------------------------|-------------------|-----------------------------------|
| Coda    | Otc     | Indo    | 7 Eleven         | 25h                    | N/A               | 1.000-\>9.999.500IDR              |
| Coda    | Otc     | Indo    | Alfamart         | 25h                    | 10.000.000IDR/day | 50.000-\>2.000.000IDR             |
| Coda    | Otc     | Indo    | Indomaret        | 25h                    | N/A               | 50.000-\>5.000.000IDR             |

Step 1: Chọn 1 trong 2 cửa hàng tiện ích

![](media/33483817d2a62f36ba801b27c76071ae.png)

Step 2: Điền số điện thoại

Step 3: Click “Lanjut/Continue” để tiếp tục.

Step 4: WP trả về trang kết quả giao dịch với thông báo như sau:

-   message của Alfamart tại trang Kết quả giao dịch:

![](media/116b86e3b43f2deabf8b7a4db2b90d17.png)

-   message của Infomaret tại trang Kết quả giao dịch:

![](media/0fa556072ba64a73120fe0da6694d867.png)

Step 5: Đồng thời rên điện thoại sẽ nhận được thông báo của Alfamart

-   1 second ago  
    CODAPAY: Ke Alfamart dlm 24 jam.Beritahukan kasir buka e-transaction
    terminal, pilih 2 PEMBAYARAN, 2, 2- CODA. Kode pembayaran
    8888820502345342.(To Alfamart in 24 hours. Tell the cashier to open the
    e-transaction terminal, choose 2 PAYMENT, 2, 2- CODA. Payment code
    8888820502345342.)  
    CODAPAY: Ke Indomaret dlm 24 jam.Beritahukan kasir buka Payment Point
    terminal, cari biller Codashop. Kode pembayaran 85820111.

Step 6: Lưu lại Payment Code và mang đến các Mini markets của Indonesia trong
vòng 24h để các nhân viên sẽ giúp đỡ bạn thanh toán với hóa đơn Codashop.

**4. Thanh toán qua Ngân hàng (Payment Partner: Coda, Channel: Banking)**

\* Mức hỗ trợ giá tham khảo:

| Partner | Channel | Country | Provider | Transactions expire in | Giới hạn \$                                                    | Mệnh giá hỗ trợ (THB)          |
|---------|---------|---------|----------|------------------------|----------------------------------------------------------------|--------------------------------|
| Coda    | bank    | Indo    | BCA      | 25h                    | Rp. 10.000.000 to Rp. 50.000.000 per day depending on the bank | Minimum Price Point: 50.000 RP |
| Coda    | bank    | Indo    | BII      |                        |                                                                |                                |
| Coda    | bank    | Indo    | BNI      |                        |                                                                |                                |
| Coda    | bank    | Indo    | BRI      |                        |                                                                |                                |
| Coda    | bank    | Indo    | Mandiri  |                        |                                                                |                                |
| Coda    | bank    | Indo    | CIMB     |                        |                                                                |                                |

Step 1: Chọn 1 trong danh sách các Ngân hàng hỗ trợ

![](media/732975aab845f78a44fbb1a6979f0c0d.png)

Step 2: Điền số điện thoại

Step 3: Click “Lanjut/Continue” để tiếp tục.

Step 4: WP trả về trang kết quả với message sau:

-   BCA

![](media/9ece8dd7677e68d93f7ec3eb108dcdb8.png)

-   BII

![](media/54395900fc22836d2c473f29748d3907.png)

-   CMIB

![](media/cbb4b58f8ba1a888e40c65ee5b9e0111.png)

-   BRI

![](media/10343f1dd70f39ecedbfada7620bebde.png)

-   BNI

![](media/4fc5a085d406c4030b18e30eb1e0d586.png)

-   Mandiri

![](media/ab7d5762826faaed9575a1b31bfea8a0.png)

Step 5: Trên điện thoại sẽ nhận được message trả về

-   BCA: Select “Other Transactions”, “Transfer” then “To Other Bank Records”.
    Enter bank code 013, transfer amount Rp. 1.492.353 and no. rek. destination
    8965020590555507.

-   BII: Select Transfer, To Another Bank, enter bank code 987 followed by no.
    virtual account 668675638000. Enter transaction amount Rp. 142353. No ref.
    empty.

-   BCA sms banking: In the app, Choose “Transfer To Another Bank”, Bank
    “Permata”, input amount Rp. 742.353 and account number 8965020548190301.
    Codapay CS: 02130029907

Step 6: Lưu lại Payment Code và mang đến các cây ATM trong vòng 24h tiến hành
thanh toán

**5. Thanh toán qua Wallet**

**5a. UniPin Wallet hoặc UniPin UP Gift Card Wallet**

\* User phải đã có account Unipin hoặc tạo mới tại
www.unipin.co.id/registration, và sử dụng account đó để thanh toán mua vật phẩm
trong game.

\* Bảng so sánh Unipin Wallet (UniPin Credits, UC) và Unipin UP Gift Card Wallet
(UP Points).

|                   | Unipin Wallet (UniPin Credits, UC)                                                                                                                                                        | Unipin UP Gift Card Wallet (UP Points)                                                            |
|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| Tác dụng          | UniPin Credits để mua vật phẩm trong game                                                                                                                                                 | UP Points để mua vật phẩm trong game                                                              |
| Cách nạp (top-up) | Có thể nạp UniPin Credits bằng cách sử dụng các payment channels có sẵn (tương tự ví Zalo Pay), including Online banking, ATM transfer, SMS, Mobile payment cũng như dùng UniPin voucher. | Có thể mua UP Gift card tại các nhà phân phối chính thức ở Indonesia, chẳng hạn như Internet Café |
| Mệnh giá hỗ trợ   | Tối đa 500,000 IDR/500,000 UC                                                                                                                                                             | Tối đa 500,000 IDR/500 UP Points                                                                  |

Step 1: Chọn gói và chọn phương thức thanh toán là UniPin Wallet (hoặc UniPin Up
Gift Card Wallet)

![](media/0c054a16d7c16153953a69851c6deab3.png)

Step 2: Click “Lanjut/Continue” để tiếp tục.

Step 3: WP sẽ điều hướng sang trang đăng nhập của UniPin để mua gói vật phẩm

![](media/73bdfe9e2978ad04286a1060febaebf9.png)

**Step 5a1: nếu tài khoản thiếu UniPin Credits**, sẽ hiện thông báo yêu cầu nạp
UniPin để tiếp tục.

![](media/63ceba4c0606fa80d3cada5ea53dc54c.png)

**Step 5a2: Nếu tài khoản thiếu UP Points**, sẽ hiện thông báo yêu cầu refill
UniPin Wallet để tiếp tục

![](media/50f21b7fbedf6b085b0835c58022820e.png)

Step 5b: Nếu click vào text “Reload”, Unipin sẽ điều hướng theo các links dưới
đây để có thể nạp UC Credits (hoặc UP Points):

-   Nạp UniPin Credits: https://payment.unipin.co.id/reload/home

-   Nạp UP Points: https://paymentv2.unipin.co.id/reload/giftcard

**Step 5c: Nếu tài khoản còn đủ UniPin Credits hoặc UP Points**, sẽ cho phép
tiếp tục thanh toán, và yêu cầu nhập Security Key trước khi thanh toán. Sau khi
click Submit sẽ trả về trang kết quả của Unipin. Click “Back to VNG Ros Mobile”
để về WP.

![](media/efadf446a910479120bcd72a0f03cd21.png)

![](media/ed90e108dc1d18bf58358ec912f0b667.png)

Step 6: WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/b38fc3dc7f3aef9112ebf429e2689e82.png)

**5b. Doku Wallet**

\* Ví Doku không tạo được tại Việt Nam nên có thể contact Fajri Gustia Nanda
(team CODA) để nhờ hỗ trợ thanh toán

Step 1: Chọn gói và phương thức thanh toán là Doku Wallet

![](media/02a27f0159f7ea735dec0ea96a017d47.png)

Step 2: Điền số điện thoại và click “Lanjut/Confirm”

Step 3: Giao diện thanh toán CodaPay hiện lên, click vào hyperlink “klik
disini/click here”

![](media/dd97463f0009397f2ec75630b4e2359b.png)

Step 4: Chọn phương thức thanh toán với config như trong ảnh dưới, sau đó click
“Submit”

![](media/780b5f7b98229b58dbc61272c7d4633c.png)

Step 5: Giao dịch thành công, click “Melanjutkan ke Pedagang/Continue to
Merchant”

![](media/bd64063955500539f3681578a0851da0.png)

Step 6: WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/b38fc3dc7f3aef9112ebf429e2689e82.png)

**5c. GoPay Wallet**

\* Ví GoPay (GO-JEK) không tạo được tại Việt Nam nên có thể contact Fajri Gustia
Nanda (team CODA) để nhờ hỗ trợ thanh toán

Step 1: Chọn gói và phương thức thanh toán là GoPay Wallet

![](media/5c02ffde6f66b45f21df32dddb3c836e.png)

Step 2: Điền số điện thoại và click “Lanjut/Confirm”

Step 3: Giao diện thanh toán GoPay hiện lên, click vào hyperlink “View the QR
Code”

![](media/3d3ad3ca24b79be73659fa3953b49308.png)

![](media/55150cbe80fc2a1040ce03ac44790078.png)

Step 4: Sử dụng GoPay (GO-JEK) app để quét QR Code và thanh toán

![](media/a16b3d0cbe516f85e98859ea04ccc696.png)

Step 6: WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/b38fc3dc7f3aef9112ebf429e2689e82.png)

**5d. zGold-MolPoints Wallet**

\* Để mua ở Staging/Production có thể liên hệ trong group MOL Global – VNG.

\*Mệnh giá hỗ trợ tham khảo:

| Partner   | Channel  | Country | Provider        | Giới hạn \$ mỗi giao dịch | Giới hạn \$ | Mệnh giá hỗ trợ (IDR) |
|-----------|----------|---------|-----------------|---------------------------|-------------|-----------------------|
| molglobal | E-Wallet | Indo    | zGold MolPoints |                           | N/A         |                       |

Step 1: Chọn gói với phương thức thanh toán là E-Wallet/zGold MolPoints

![](media/cc0a64a4b8975c0d524c40ce3b53f529.png)

Step 2: Click “Confirm”

Step 3: WP điều hướng sang trang đăng nhập zGold MolPoints

![](media/6e160f2e262d26895a5c80289580e9a9.png)

![](media/e140f6c6213920e602302ffda97f8087.png)

Step 4: Login với tài khoản zGold MolPoints và tiến hành thanh toán

![](media/5030e35dfd8a042dd44daa42b92e3508.png)

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

**5e. OVO Wallet**

\* Video hướng dẫn thanh toán có thể xem tại link này:
[OVO_Wallet_Payment](https://drive.google.com/open?id=1852P5lQuCMHqKYQb9iz_je-jE5ZIozqf)

\* Để mua ở Staging/Production có thể liên hệ trong group VNG – Redision
Technical Integration.

\*Mệnh giá hỗ trợ tham khảo:

| Partner | Channel  | Country | Provider | Giới hạn \$ mỗi giao dịch | Giới hạn \$ | Mệnh giá hỗ trợ (IDR) |
|---------|----------|---------|----------|---------------------------|-------------|-----------------------|
|         | E-Wallet | Indo    | OVO      | 100000IDR                 |             |                       |

Step 1: Mở sẵn device đã đăng nhập tài khoản OVO Wallet với số điện thoại sẽ
dùng để thanh toán

Step 2: Chọn gói với phương thức thanh toán là E-Wallet/OVO

Step 3: Click “Confirm”

Step 4: WP điều hướng sang trang đăng nhập OVO Wallet, click “GO” button để đến
bước tiếp theo

Step 5: Nhập số điện thoại trùng với số điện thoại đang mở tài khoản OVO ở step
1. Sau đó click “PROCEED” button

Step 6: Mở device và click button “Thanh toán” trên device (lưu ý chỉ có 30s để
thực hiện thao tác này), sau 30s thì giao dịch trên Web Payment sẽ bị time-out.

Step 7: Trên device báo giao dịch thành công và bị trừ tiền. Web Payment cũng
đồng thời thông báo giao dịch thành công.

Step 8: Click “CLOSE” button trên Web Payment để trở về trang kết quả giao dịch.
Vô game để nhận items.

**6. Thanh toán qua Cashcard**

**6a. Thanh toán qua UniPin Express hoặc UniPin UP Gift Card Express**

\* User không cần có account Unipin. Thay vào đó có thể đến các
Minimarket/Convenient Store để mua UniPin Voucher (có chứa Serial và Pin code).

\*Mệnh giá tham khảo cho UniPin Express/UniPin UP Gift Card Express:

-   10.000IDR

-   20.000IDR

-   50.000IDR

-   100.000IDR

-   300.000IDR

-   500.000IDR

Step 1: Chọn gói và chọn phương thức thanh toán Cashback là UniPin Express (hoặc
UniPin Up Gift Card Express)

![](media/bb34b603491712e04a50e8cfbcd77c90.png)

Step 2: Điền Serial ở trên và Pin code ở dưới

Template for Serial: IDMB-X-S-XXXXXXXX hoặc UPGC-X-S-XXXXXXXX (thay X bằng số
chính xác)

Template for Pin: XXXX-XXXX-XXXX-XXXX (thay X bằng số chính xác)

Step 3: Click “Lanjut/Continue” để tiếp tục.

Step 4: WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/06c17529f5d79aed30059361ea15c3d1.png)

**6b. Thanh toán qua zGold-MolPoints Cashcard**

Step 1: Chọn gói và chọn phương thức thanh toán Cashback là zGold-MolPoints

Step 2: WP sẽ redirect users đến trang nhập thông tin thẻ zGold-MolPoints

Step 3: Điền thông tin thẻ zGold-MolPoints

Step 4: Click “Lanjut/Continue” để thanh toán.

![](media/5c36e0dd8b2ba99227929f4b637278b4.png)

Step 5: Trang zGold-MolPoints thông báo thanh toán thành công, click
“Close/Đóng” để back trở lại WP result

![](media/767827a6a48ba74d74305104bd07183f.png)

Step 6: WP trả về kết quả giao dịch thành công, users được nhận items, tiền dư
sẽ vào ví tồn.

![](media/06c17529f5d79aed30059361ea15c3d1.png)
