#### [QC] General Requirements to request QC Test

July 22, 2019 [General
Requirement](https://wiki.mto.zing.vn/category/gt-qc/general-requirement/)

I. Permission

1.  Who can access site/tool? All VNG-ers, people with ACL, people with
    permission grant

2.  Roles in site/tool (if having)? Admin, User,…

II. Environment

1.  Link to test, Tool to test?

2.  Types of environments: Sandbox, Staging, Production

III. Types of Test Required?

1.  API Test?

2.  Functional Test?

3.  UI Test?

4.  Automation Test?

5.  Performance Test?

6.  Regression Test? (Test lại chức năng hay test mới)

IV. Timelines

1.  When can start the test? (ex: 01-Aug-2019)

2.  Deadline of product? (ex: 10-Oct-2019)

3.  Some milestones (if having) during product? (ex: 15-Aug-2019 -\> báo cáo
    tiến độ lần 1, 1-Sep-2019 -\> báo cáo tiến độ lần 2,…)

V. Send Test Request

1.  Send to mailing list: gtqc\@vng.com.vn
