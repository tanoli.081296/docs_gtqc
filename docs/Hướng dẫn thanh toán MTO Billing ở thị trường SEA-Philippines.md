#### [SEA][Philippines] Hướng dẫn thanh toán MTO Billing ở thị trường SEA-Philippines

July 18, 2018 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/)

Philippines sử dụng Peso Philippines (PHP)

Giai đoạn 1:

-   Coda (ưu tiên làm trước): mở hết các kênh đang có của Coda:

    -   SMS: Smart, Globe

    -   Banks

    -   E-wallet: Gcash

    -   OTC: 7-11, Robinson, Bayad

-   MOL (làm sau Coda, tuy phải tích hợp mới với MOL Global API, nhưng họ
    support local marketing nên mình ưu tiên tích hợp luôn trong giai đoạn 1):

    -   MOL e-wallet

    -   MOL direct topup (chờ GBD deal lại API redeem amount với MOL, nếu họ ko
        fix dc, mình ko mở kênh này)

Giai đoạn 2:

-   2 bạn E-wallet mới: chờ confirm của GBD.

Hướng dẫn mọi người cách tiến hành login và thanh toán MTO Billing ở thị trường
SEA-Thailand (Trong bài viết là ví dụ với game Rules Of Survival)

**1. Thanh toán qua Ví Tồn (MTO Wallet) của Web Payment**

\* Nếu tiền tồn lại trong ví của user đủ để mua gói đang chọn (ví dụ ví tồn của
user đang có 925 PHP, và chọn mua gói 30 PHP) thì chỉ WP sẽ hiện phương thức
thanh toán là **Ví Tồn (MTO Wallet)** để user có thể sử dụng tiền tồn trong ví
thanh toán, thay vì tiến hành các phương thức thanh toán khác.

Step 1: Proceed để ví tồn còn tiền

Step 2: Mua gói nhỏ hơn ví tồn còn lại

Step 3: WP điều hướng đến lựa chọn Account Balance.

Step 4: Click Confirm để xác nhận thanh toán

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/586dc061bc68d6702ac8cc44c341f0b9.png)

\* Nếu tiền tồn tại trong ví của user thấp hơn gói đang lựa chọn (ví dụ còn 6THB
nhưng chọn mua gói 30THB), thì sẽ có 1 pop-up thông báo để user click
“Confirmed” và tiếp tục thanh toán với các phương thức thanh toán dưới đây:

![](media/564ed1de039104a5701fa28f1c3d4f66.png)

**2. Thanh toán qua SMS**

-   Hiện có 1 nhà cung cấp (Providers) cho dịch vụ thanh toán qua SMS là CODA.
    Cần xác định xem các phương thức SMS đang sử dụng là thuộc Providers nào
    (nếu có bổ sung) để nhập thông tin thẻ chính xác.

Mẫu số điện thoại tham khảo:

-   Smart: 09189000100, 09186216355, (+639186216355,+639189000100)

-   Globe: 09158493035, 09952469342 (+639158493035, +639952469342)

Thanh toán qua SMS của Provider – CODA

\*Mức giá hỗ trợ tham khảo CODA:

| Partner | Channel | Country | Provider | Transactions expire in | Giới hạn \$ | Mệnh giá hỗ trợ (PHP) |
|---------|---------|---------|----------|------------------------|-------------|-----------------------|
| CODA    | SMS     | Myanmar | Smart    |                        |             |                       |
| CODA    | SMS     | Myanmar | Globe    |                        |             |                       |
|         |         |         |          |                        |             |                       |

Step 1: Vào trang thanh toán WP RosThai (ex:
https://sandbox-billing.mto.zing.vn/webshop/rosmym/\#/pay)

Step 2: Chọn gói và phương thức thanh toán là SMS

Step 3: Chọn 1 trong 3 nhà mạng Smart/Globe, sau đó click Confirmed.

![](media/8386ee5b04b53eb216af06ce977c0ff0.png)

![](media/08feafa5212ff44ffef25cbe4f54e014.png)

Step 4a: Với nhà mạng Smart, WP sẽ điều hướng ra trang result, với message hướng
dẫn users soạn tin nhắn từ devices đến số của tổng đài để thanh toán (có kèm
thông tin Payment ID). Trên Sandbox/Staging/Production, hãy nhờ đối tác bên
Philipines nhắn tin hộ để có thể kiểm tra Giao dịch thành công.

![](media/eafabc2edfc4d62fba712d5ff4085ed2.png)

Step 4b: Với nhà mạng Globe, WP sẽ điều hướng ra trang thanh toán của
CodaPayments.com, user điền số điện thoại. Sau đó nhập mã 3-digit để xác nhận
thanh toán.

![](media/760bdbcd4aed63a518debf4d9918802b.png)

![](media/104e919099eb0083152960edc25aeda4.png)

Step 5a: Với Sandbox environment, có thể điền 777 và click Confirm để giao dịch
thành công. Nếu environment khác, vui lòng tham khảo 2 bước dưới.

Step 5b: Với Production/Staging environment, trên devices, mở trình soạn thảo
văn bản (tin nhắn sms) với nội dung như hướng dẫn ở trang result gửi đến tổng
đài. Hãy nhờ đối tác bên Philipines hỗ trợ gửi tin nhắn.

Step 6: Sau khi thanh toán thành công, quay trở lại WP. Click “Check the payment
results”, WP sẽ refresh và trả về kết quả giao dịch, users đ

![](media/711d5ed3c602c7b5d50034bf698a017a.png)

ược nhận items, tiền dư sẽ vào ví tồn.

**3. Thanh toán qua OTC – Mini market/Convenient Store. (Payment Partner: Coda,
Channel: Otc)**

\*Philipines có 3 thương hiệu chính là 7-11, Robinson, Bayad

\* Mức hỗ trợ giá tham khảo:

| Partner | Channel | Country    | Network Provider | Transactions expire in | Giới hạn \$ | Mệnh giá hỗ trợ (đơn vị: PHP |
|---------|---------|------------|------------------|------------------------|-------------|------------------------------|
| Coda    | Otc     | Philipines | 7 Eleven         |                        |             |                              |
| Coda    | Otc     | Philipines | Robinson         |                        |             |                              |
| Coda    | Otc     | Philipines | Bayad            |                        |             |                              |

Step 1: Chọn 1 trong 3 cửa hàng tiện ích

![](media/39f3b7556f6fc9b7db4b016cb66c95b3.png)

Step 2: Điền số điện thoại

Step 3: Click “Submit” để tiếp tục.

Step 4: WP trả về trang kết quả giao dịch với thông báo mở Email để xem hướng
dẫn nạp tiền qua hình thức OTC:

![](media/946617610ddfc10eae1397bdf7e19e81.png)

Step 5a: Với Sandbox environment, có thể truy cập link sau:
<https://stage.codapayments.com/epcsimulator/>

và làm theo hướng dẫn như trong ảnh, Trans Id Merchant này là hệ thống của đối
tác lưu trữ, để có thể lây được, sau mỗi giao dịch, hãy đưa Order Number hiện
trên WebPay, rồi nhờ bạn HuuNV tìm giúp Trans Id Merchant

![](media/749609bf0606f1238aef658d7be1aa43.png)

Step 5b: Với Production/Staging environment, một Email với hướng dẫn sẽ được gửi
tới User, vui lòng tuân theo hướng dẫn để tiến hành thanh toán. Hãy nhờ đối tác
bên Philipines đi đến các điêm giao dịch (local stores) ở Philipines để Hoàn tất
giao dịch.

**4. Thanh toán qua E-Wallets**

**4a. Thanh toán qua E-Wallets GCash**

\* Để mua ở Staging/Production có thể liên hệ Fajri Gustia Nanda trong group
CODA – VNG để mượn tài khoản hoặc nhờ test hộ. Tutorial Video có thể tham khảo ở
.

\*Mệnh giá hỗ trợ tham khảo:

| Partner | Channel  | Country    | Provider | Giới hạn \$ mỗi Tnx | Giới hạn \$ | Mệnh giá hỗ trợ (PHP) |
|---------|----------|------------|----------|---------------------|-------------|-----------------------|
| CODA    | E-Wallet | Philipines | GCash    |                     |             |                       |

Step 1: Chọn gói với phương thức thanh toán là E-Wallet/GCash

![](media/d6e9f173a07a9cb3a7d2d7cb7fe167c5.png)

![](media/ee49fd94c8e06841fa5ac440b0f5cecd.png)

Step 2: Nhập số điện thoại, sau đó Click “Confirm”

Step 3: WP điều hướng sang trang thanh toán CodaPayments.com, tiếp tục nhập số
điện thoại và click “Continue”

![](media/f6a13812d8b57509e3c9f0cfde1654f6.png)

![](media/0109c8607249fa256805c8739488bbfd.png)

Step 4: Đến bước này User sẽ bị chuyển qua trang thông tin của cổng thanh toán
DragonPay.PH

![](media/213bfd2fe040102e891098386bd4f029.png)

Step 5: Với Sandbox environment, vô trang liên kết sau:
<https://stage.codapayments.com/epcsimulator/dragonPayNotification.action>, điền
các thông tin tương tự như ảnh dưới, riêng chỉ có Trans ID là lấy ở URL tại Step
2

![](media/749609bf0606f1238aef658d7be1aa43.png)

Step 6: Quay lại link ở Step 3, sẽ nhận được thông báo Giao dịch thành công,
click Close để kết thúc giao dịch.

![](media/ee49fd94c8e06841fa5ac440b0f5cecd.png)

Step 5b: Với Production/Staging environment, điền account Gcash Wallet (nhờ đối
tác bên Philipines có acc Gcash Wallet hỗ trợ test)

Step 7: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

**4b. Thanh toán qua E-Wallets zGold-MOLPoints Wallet**

\* Để mua ở Staging/Production có thể liên hệ trong group để mượn tài khoản hoặc
nhờ test hộ. Tutorial Video có thể tham khảo ở .

\*Mệnh giá hỗ trợ tham khảo:

| Partner   | Channel  | Country    | Provider        | Giới hạn \$ mỗi Tnx | Giới hạn \$ | Mệnh giá hỗ trợ (PHP)              |
|-----------|----------|------------|-----------------|---------------------|-------------|------------------------------------|
| molglobal | E-Wallet | Philipines | zGold MolPoints |                     |             | \$1, \$5, \$10, \$50, \$100, \$250 |

Step 1: Chọn gói với phương thức thanh toán là E-Wallet/zGold MolPoints

![](media/35f103b79f1cfeda28cf4c724106768b.png)

Step 2: Click “Confirm”

Step 3: WP điều hướng sang trang đăng nhập zGold MolPoints

![](media/9a9cafdb7078d21ff0655ad4d08649f2.png)

Step 4: Login với tài khoản zGold MolPoints và tiến hành thanh toán

-   Sandbox environment: Hãy mượn acc trong group MOL Global – VNG

-   Production environment: Có thể tự tạo acc, nạp tiền vô ví, và tiến hành
    thanh toán bằng tiền thật.

![](media/74e8965c4f3c6f5a0f8c51adb0924633.png)

Step 5: Trang globalmol.com sẽ trả về kết quả thành công, click “Back to
merchant’s site” để về lại giao diện WP thanh toán

![](media/90f2a29babaf81e82898bb8fda8f0710.png)

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/1d746f1158f5e6f146de2b67d25a5992.png)

**5. Thanh toán qua Cashcard (zGold-Molpoint Direct Top Up)**

\* Để mua ở Staging/Production có thể liên hệ trong group để mượn tài khoản hoặc
nhờ test hộ. Tutorial Video có thể tham khảo ở .

\*Mệnh giá hỗ trợ tham khảo:

| Partner   | Channel  | Country    | Provider        | Giới hạn \$ mỗi Tnx | Giới hạn \$ | Mệnh giá hỗ trợ (PHP) |
|-----------|----------|------------|-----------------|---------------------|-------------|-----------------------|
| molglobal | Cashcard | Philipines | zGold MolPoints |                     |             |                       |

Step 1: Chọn gói với phương thức thanh toán là Cashcard/zGold-Molpoint Direct
Top Up

Step 2: Click “Confirm”

Step 3: WP điều hướng sang trang API điền Serial/PIN để top up trực tiếp

Step 4: Tiến hành thanh toán

-   Sandbox environment: Liên hệ manhtt hoặc nhutbm để lấy thẻ ảo sử dụng được
    trên Sandbox

-   Production environment: Có thể mua trực tiếp tại trang
    <https://www.seagm.com/razer-gold-philippines>

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

**6. Thanh toán qua Banks**

\* Để mua ở Staging/Production có thể liên hệ Fajri Gustia Nanda trong group
CODA – VNG để mượn tài khoản hoặc nhờ test hộ. Tutorial Video có thể tham khảo ở
.

\*Mệnh giá hỗ trợ tham khảo:

| Partner | Channel | Country | Provider      | Tnx expire in | Giới hạn \$ | Mệnh giá hỗ trợ (PHP) |
|---------|---------|---------|---------------|---------------|-------------|-----------------------|
| CODA    | bank    | Phi     | May Bank      |               |             |                       |
| CODA    | bank    | Phi     | PNB           |               |             |                       |
| CODA    | bank    | Phi     | East West     |               |             |                       |
| CODA    | bank    | Phi     | UCPB          |               |             |                       |
| CODA    | bank    | Phi     | RCBC          |               |             |                       |
| CODA    | bank    | Phi     | Union Bank    |               |             |                       |
| CODA    | bank    | Phi     | Land Bank     |               |             |                       |
| CODA    | bank    | Phi     | China Bank    |               |             |                       |
| CODA    | bank    | Phi     | BPI           |               |             |                       |
| CODA    | bank    | Phi     | Sterling Bank |               |             |                       |
| CODA    | bank    | Phi     | Security bank |               |             |                       |
| CODA    | bank    | Phi     |               |               |             |                       |
| CODA    | bank    | Phi     |               |               |             |                       |

Step 1: Chọn gói với phương thức thanh toán là Banks

Step 2: Chọn Bank bạn muốn sử dụng để thanh toán, điền Email sau đó click
“Confirm”

![](media/a54d55cf168b9b927dc4a6587fbd2ef8.png)

Step 5: WP trả về trang kết quả giao dịch với thông báo đã gửi hướng dẫn thanh
toán đến Email.

![](media/9cf87acd560ef4fce9a0355431c589c4.png)

Step 6a: Trên Sandbox environment, có thể truy cập link sau:
<https://stage.codapayments.com/epcsimulator/>

và làm theo hướng dẫn như trong ảnh, Trans Id Merchant này là hệ thống của đối
tác lưu trữ, để có thể lây được, sau mỗi giao dịch, hãy đưa Order Number hiện
trên WebPay, rồi nhờ bạn HuuNV tìm giúp Trans Id Merchant

Step 6b: Trên Staging/Production environment: Một Email với hướng dẫn sẽ được
gửi tới User, vui lòng tuân theo hướng dẫn để tiến hành thanh toán.
