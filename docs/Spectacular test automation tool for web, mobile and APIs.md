#### [Open Test] Spectacular test automation tool for web, mobile and APIs

July 1, 2019 [Automation](https://wiki.mto.zing.vn/category/gt-qc/automation/)

Installation
------------

Prerequisites: [Node.js 8](https://nodejs.org/en/) or greater; [Java runtime
8](https://java.com/en/download/) or greater.

Run the command below to install OpenTest from npm:

npm install opentest -g

Quick start
-----------

This section shows the steps to create a default OpenTest configuration and run
the first test session.

| NOTE | These instructions are for Windows users. For macOS or Linux you must adjust the paths accordingly. |
|------|-----------------------------------------------------------------------------------------------------|


1.  Create a directory named opentest on the C: drive to use as a playground for
    this guide, and cd into that directory.C: md C:\\opentest cd C:\\opentest

2.  Run the command belowopentest quick-startYour opentest directory should now
    look like this:C:\\opentest ├── server │ └── server.yaml ├── actor1 │ └──
    actor.yaml └── test-repo ├── macros │ └── ... ├── scripts │ └── ... ├──
    templates │ └── ... └── tests └── ...The server and the actor1 directories
    are going to be used as the working directories for the OpenTest server and
    default test actor, respectively. The test-repo directory is a sample test
    repository that demonstrates some basic features of OpenTest.

3.  Open a new terminal window, cd to the server’s working directory and start
    the OpenTest server:cd C:\\opentest\\server opentest serverNOTEThe OpenTest
    server (aka the sync server) is a Node.js application who’s main functions
    are to orchestrate test execution and expose the OpenTest web-based UI.

4.  Open a new terminal window, cd to the actor’s working directory and start
    the test actor:cd C:\\opentest\\actor1 opentest actorNOTEThe test actor is a
    Java application that runs on the system under test and executes test steps
    as instructed by the sync server.

5.  Open a web browser and navigate to
    [http://localhost:3000](http://localhost:3000/).

6.  From the menu, select **Session \> Create Session From Template**. Select
    the template **Quick start tests** and click **Create session**. Wait a few
    seconds for the test session to finish and click on it to examine the test
    results and log information.

Updating OpenTest to a new version
----------------------------------

To update your OpenTest installation to a new version, run the same command you
used to install it:

npm install opentest -g

If you are updating to a new major version, however, you need to explicitly
specify the version number you are updating to in your npm install command. For
example, if you are currently running OpenTest 0.9.16 and you are updating to
1.0.4, you need to run the command:

npm install opentest\@1.0.4 -g

Alternatively, to update to the latest stable version available, you can do:

npm install opentest\@latest -g

Source: <https://getopentest.org/docs/installation.html>
