#### [2019-04-17] QC How-to create Test Case

May 18, 2019 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/), [Project
Requirement](https://wiki.mto.zing.vn/category/gt-automation-ga/project-requirement/),
[Project
Requirement_QC](https://wiki.mto.zing.vn/category/mto-billing/project-requirement_qc/)

I. HOW TO CREATE TEST CASE

1. Summary/Title of TC:

>   Format: **[Epic][Component/Module][Label]** Content of TC

>   Ex: [Payment_VN][Login][Zing ID] User can log in successfully

>   **Epic:** Payment_VN, Payment_ID, Payment_PH, Payment_TH, Payment_MM

>   **Component:** Login, ATM, CC(Credit Card), Pre-paird Card, e-Wallet,
>   mto-wallet, SMS,

>   **Label:** ZingID, Zalo, Google, Facebook, ZingMe, Line, Email(Quick_Login),
>   RoleId, **[Mobile Banking, Online Banking], [MasterCard, VisaCard],
>   [ZingCard, MobiCard, VinaCard, ViettelCard, …], [ZaloPay, …], [MobiSMS,
>   VinaSMS, ViettelSMS, ]**

1. Description: Hide

2. Attachment: Hide

3. Priority

High: Full TC + Regression Test

Medium: Full TC

Low: Full TC

Full TC: Game moi

Regression Test: Game da ra

4. Impact: Hide

5. Assignee: Hide

6. Epic Link: Hide

7. Environment: Sandbox, Staging…

8. Affect Version: Hide

9. Resolution: Hide

10. Fix Version: hide

11. Approvers: hide

12. Start date: hide

13. End date: Hide

14. Sprint: hide

II. Test Cycle

1.  Sandbox -\> Full

2.  Staging/Production -\> Quick (High Priority)

3.  Chrome -\> Full

4.  Safari -\> Quick (High Priority)

5.  IE/Edge: Optional (Chot sau)

6.  PC: Full

7.  Mobile: Full

III. Report

1.  Pass: 15 TC

2.  Fail: 1 TC …

3.  Bug ID: ….

IV. TC

1.  Review TC

2.  Nhan – TC VN

3.  Nhut – TC TH/ID/MM/

4.  Main Flow

5.  Happy cases

6.  Estimate TC: 4h (ex: 100 TC)

7.  Time to write TC (excel): 15p/1TC

8.  Time to push online: 10p/TC

9.  [Mobile] & TC
