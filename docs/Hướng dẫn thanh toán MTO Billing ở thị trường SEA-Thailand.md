#### [SEA][Thailand] Hướng dẫn thanh toán MTO Billing ở thị trường SEA-Thailand

April 18, 2018 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/)

Hướng dẫn mọi người cách tiến hành login và thanh toán MTO Billing ở thị trường
SEA-Thailand (Trong bài viết là ví dụ với game Rules Of Survival)

**I. Login**

1. Option 1 – Login with character name:

![](media/040def7816c37545059ff46904e81778.png)

Step 1a: Chọn server

Step 1b: Điền username của người chơi, lưu ý điền đúng chữ cái in hoa, in thường
(tài khoản Facebook sẽ có id khác tài khoản facebook, fACEbook, etc…)

2. Option 2 – Login with RoleID:

\*RoleID không phải ID trong game, mà là RoleID của WP, có thể thấy RoleID sau
khi login with character name.

![](media/3eff621eb6ab1414c447c2feb3517c5d.png)

Step 2a: Chọn server

Step 2b: Điền RoleID

**II. Payment Processing**

**1. Thanh toán qua Ví Tồn (MTO Wallet) của Web Payment**

\* Nếu tiền tồn lại trong ví của user đủ để mua gói đang chọn (ví dụ ví tồn của
user đang có 925 THB, và chọn mua gói 30 THB) thì chỉ WP sẽ hiện phương thức
thanh toán là **Ví Tồn (MTO Wallet)** để user có thể sử dụng tiền tồn trong ví
thanh toán, thay vì tiến hành các phương thức thanh toán khác.

![](media/be228e5e0a44179effd28927ea29694d.png)

![](media/1cedd083b3fc9faea682e892c7f27e0d.png)

Step 1: Proceed để ví tồn còn tiền

Step 2: Mua gói nhỏ hơn ví tồn còn lại

Step 3: WP điều hướng đến lựa chọn Account Balance.

Step 4: Click Confirm để xác nhận thanh toán

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/586dc061bc68d6702ac8cc44c341f0b9.png)

\* Nếu tiền tồn tại trong ví của user thấp hơn gói đang lựa chọn (ví dụ còn 6THB
nhưng chọn mua gói 30THB), thì sẽ có 1 pop-up thông báo để user click
“Confirmed” và tiếp tục thanh toán với các phương thức thanh toán dưới đây:

![](media/564ed1de039104a5701fa28f1c3d4f66.png)

**2. Thanh toán qua SMS**

\* Test trên Sandbox: Chưa có cách nào

\* Test trên Production: Sử dụng số điện thoại của nhà mạng Thailand để thanh
toán.

-   Hiện nhà cung cấp (Providers) cho dịch vụ thanh toán qua SMS là Razer Gold.
    Cần xác định xem các phương thức SMS đang sử dụng là thuộc Providers nào để
    nhập thông tin thẻ chính xác.

Mẫu số điện thoại tham khảo:

AIS: 0925515573

Dtac: 0993981288

TrueMoney: 0917207122

\*Mức giá hỗ trợ tham khảo Razer Gold:

| Channel | Country | Provider   | Mệnh giá hỗ trợ (THB)   |
|---------|---------|------------|-------------------------|
| SMS     | Thai    | Razer Gold | 25, 30, 60, 90, 150,300 |

Step 1: Vào trang thanh toán WP RosThai (ex:
https://sandbox-billing.mto.zing.vn/webshop/rosthai/\#/pay)

Step 2: Chọn gói và phương thức thanh toán là SMS

Step 3: Click Confirmed. WP sẽ điều hướng ra trang Razer Gold,

Step 4: Nhập chính xác số điện thoại sa đó click Continue

Step 5 : Razer Gold trả về message hướng dẫn users soạn tin nhắn từ devices đến
số của tổng đài để thanh toán

![](media/3990137e759131467c8113cc3cf681f1.png)

Step 6: Trên devices, mở trình soạn thảo văn bản (tin nhắn sms) với nội dung như
hướng dẫn ở trang result gửi đến tổng đài

Step 7: Sau khi thanh toán thành công, quay trở lại WP. Click “Check the payment
results”, WP sẽ refresh và trả về kết quả giao dịch, users được nhận items, tiền
dư sẽ vào ví tồn.

![](media/711d5ed3c602c7b5d50034bf698a017a.png)

**3. Thanh toán qua Cashcard**

\* Test trên Sandbox: Liên hệ **manhtt\@vng.com.vn** hoặc **nhutbm\@vng.com.vn**
để cung cấp thẻ sandbox

\* Test trên Production: Truy cập trang “<https://www.seagm.com/>“, tạo account
và search thẻ cần mua

-   Hiện có 2 nhà cung cấp (Providers) cho dịch vụ thanh toán Cashcard là
    MolThai và BluePay. Cần xác định xem các phương thức Cashcard đang sử dụng
    là thuộc Providers nào để nhập thông tin thẻ chính xác.

3a. Thanh toán qua Cashcard BluePay

\* Mệnh giá hỗ trợ tham khảo

| Partner | Channel           | Country | Provider  | Giới hạn \$ mỗi giao dịch | Giới hạn \$ | Mệnh giá hỗ trợ (THB)             |
|---------|-------------------|---------|-----------|---------------------------|-------------|-----------------------------------|
| Bluepay | CashCard Thailand | Thai    | 12call    | 50-\>1000 THB             | unlimited   | 60, 90, 150, 300, 600             |
| Bluepay |                   |         | MolPoints | 50-\>5000 THB             | unlimited   | 60, 90, 150, 300, 600, 1500, 3000 |
| Bluepay |                   |         | truemoney | 10-\>1000 THB             | unlimited   | 60, 90, 150, 300, 600             |

\*Các cách thức mua thẻ nạp Cashcard để thanh toán thật trên Production/Staging:

-   Mua thẻ ở các cửa hàng 7-Eleven (chỉ bán TrueMoney) hoặc Family Mart

-   Nhà phát hành thẻ: 12Call (AIS), TrueMoney (TrueMove)

-   Mua qua các ví điện tử như [BluePay
    Wallet](https://play.google.com/store/apps/details?id=asia.bluepay.client&hl=en);
    hoặc các trang web bán thẻ như: [1-2-call](https://www.seagm.com/1-2-call),
    [truemoney-th](https://www.seagm.com/truemoney-th)

Step 1: Chọn phương thức thanh toán là Cashcard

Step 2: Chọn thẻ của 1 trong các nhà cung cấp (AIS 12Call, BluePay BlueCoins,
TrueMoney, MolPoints)

Step 3: Click Confirm WP sẽ điều hướng sang https://sea-web.gold.razer.com

Step 4: Nhập mã PIN của thẻ (mã nạp tiền)

![](media/462c61877cd001b7fe8c7dd9f5410e0e.png)

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/e227e3b61b8e29eeba0fa5d269856345.png)

![](media/058dd72fdb6767fff5a717552729afd6.png)

\*\*\*Nếu thực hiện theo các bước trên mà báo lỗi: **Sorry, this service does
not support for your country:VN!.** Xem bài hướng dẫn sau

<https://wiki.mto.zing.vn/2020/03/27/huong-dan-test-o-thi-truong-sea-khi-co-thong-bao-sorry-this-service-does-not-support-for-your-countryvn/>

3b. Thanh toán qua Cashcard Molthailand

-   Ex: Để mua thẻ do ở Staging/Production có thể liên hệ skype: Supachai
    Gerdpholwattana hoặc Sasiprapa Fon trong group MOL TH – VNG.

\* Mệnh giá hỗ trợ tham khảo:

\*Các cách thức mua thẻ nạp Cashcard để thanh toán thật trên Production/Staging:

-   Mua qua các trang web bán thẻ như:
    [zgold-molpoints-thailand](https://www.seagm.com/zgold-molpoints-thailand)

**4. Thanh toán qua E-Wallets True Money**

\* Test trên Sandbox: Chưa có cách nào.

\* Test trên Production: Liên hệ Supachai Gerdpholwattana hoặc Sasiprapa Fon
trong group MOL TH – VNG để mượn tài khoản hoặc nhờ test hộ. Tutorial Video có
thể tham khảo ở [link dưới
đây](https://drive.google.com/open?id=128znFs0nxNzYABC3olOL8GNnfkpmRVkq).

\*Mệnh giá hỗ trợ tham khảo:

| Partner | Channel  | Country | Provider         | Giới hạn \$ mỗi giao dịch | Giới hạn \$ | Mệnh giá hỗ trợ (THB)                 |
|---------|----------|---------|------------------|---------------------------|-------------|---------------------------------------|
| MolThai | E-Wallet | Thai    | TrueMoney Wallet | N/A                       | 30kB        | 30, 60, 90, 150, 300, 600, 1500, 3000 |

Step 1: Chọn gói với phương thức thanh toán là E-Wallet/TrueMoney

Step 2: Click “Confirm”

Step 3: WP điều hướng sang trang đăng nhập TrueMoney

![](media/91684b039eb3cbdcdc64c113d22a04e0.png)

Step 4: Login với tài khoản TrueMoney và tiến hành thanh toán

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

**5. Thanh toán qua Banks**

\* Test trên Sandbox: Theo các steps bên dưới.

\* Test trên Production: Cần nhập thông tin thẻ ATM hoặc tài khoản iBanking của
Thailand để test.

\* Để mua ở Staging/Production có thể liên hệ Supachai Gerdpholwattana hoặc
Sasiprapa Fon trong group MOL TH – VNG để nhờ test hộ. Tutorial Video có thể
tham khảo [Link dưới
đây](https://drive.google.com/open?id=1aJPXBpWjesC9K2b0y1keXEnluoE2s-SH)

\* Mức hỗ trợ giá tham khảo:

| Partner | Channel | Country | Provider        | Transactions expire in | Giới hạn \$                   | Mệnh giá hỗ trợ (THB)                           |
|---------|---------|---------|-----------------|------------------------|-------------------------------|-------------------------------------------------|
|         | bank    | Thai    | SCB (Siam)      |                        | per day depending on the bank | Minimum Price Point: 20THB Fee: 10-25 per tranx |
|         | bank    | Thai    | KTB (Krungthai) |                        |                               |                                                 |
|         | bank    | Thai    | BAY (Krungsri)  |                        |                               |                                                 |
|         | bank    | Thai    | BBL (Bangkok)   |                        |                               |                                                 |

Step 1: Chọn gói với phương thức thanh toán là Banks

Step 2: Chọn Bank bạn muốn sử dụng để thanh toán, sau đó click “Confirm”

![](media/e4fc8f6c97c7577dcdf419acb24a0f24.png)

Step 3a: Trên Sandbox: WP điều hướng sang trang thanh toán giả lập, hoặc

![](media/8183ca6d6f03145724ecb8d33f241a98.png)

Step 3b: Trên Production server: WP điều hướng sang trang thanh toán của Banks
để users nhập thông tin thẻ:

a. SCB

![](media/ec4cdbce177face45431e366b7dcfded.png)

b. KTB

![](media/e60ea0b96b5e648ca242b09eb0cf7e67.png)

c. BAY

![](media/a135fde37aad14371b38d60330baefba.png)

d. BBL

![](media/13a49abb263f586f8726bdd1d0ac900d.png)

Step 4a : Trên Sandbox: Click “Success” để giao dịch thành công và cộng tiền vào
tài khoản users.

Step 4b : Trên Product: Điền các thông tin được yêu cầu từ ngân hàng để tiến
hành thanh toán.

Step 5: WP trả về kết quả giao dịch, users được nhận items, tiền dư sẽ vào ví
tồn.

![](media/711d5ed3c602c7b5d50034bf698a017a.png)

**6. Thanh toán qua Mobile Banking & Counter**

\* Test trên Sandbox: Theo các steps bên dưới.

\* Test trên Production: Cần nhập thông tin thẻ ATM hoặc tài khoản iBanking của
Thailand để test.

-   Mức giá hỗ trợ tham khảo:

| Partner | Channel | Country | Provider        | Transactions expire in | Giới hạn \$                   | Mệnh giá hỗ trợ (THB)                           |
|---------|---------|---------|-----------------|------------------------|-------------------------------|-------------------------------------------------|
|         | bank    | Thai    | SCB (Siam)      |                        | per day depending on the bank | Minimum Price Point: 20THB Fee: 10-25 per tranx |
|         | bank    | Thai    | KTB (Krungthai) |                        |                               |                                                 |
|         | bank    | Thai    | BAY (Krungsri)  |                        |                               |                                                 |
|         | bank    | Thai    | BBL (Bangkok)   |                        |                               |                                                 |
|         | bank    |         | CIMB            |                        |                               |                                                 |
|         | bank    |         | KTB             |                        |                               |                                                 |
|         | bank    |         | KBANK           |                        |                               |                                                 |
|         | bank    |         | TBANK           |                        |                               |                                                 |
|         | bank    |         | TMB             |                        |                               |                                                 |
|         | OTC     |         | BIGC OTC        |                        |                               |                                                 |
|         | OTC     |         | Familymart OTC  |                        |                               |                                                 |
|         | OTC     |         | mPAY OTC        |                        |                               |                                                 |
|         | OTC     |         | PAY\@POST OTC   |                        |                               |                                                 |
|         | OTC     |         | TESCO – OTC     |                        |                               |                                                 |
|         | OTC     |         | CenPay – OTC    |                        |                               |                                                 |

**6a. Thanh Toán trên Sandbox, Staging:**

Step 1: Chọn gói với phương thức thanh toán là Mobile Banking & Counter

Step 2: Click “Confirm”. WP sẽ điều hướng sang trang thanh toán CODAPAY. Copy id
trên URL như hình:

![](media/d9a33352ab4c5802ed06ded9153e1526.png)

Step 3: Đi tới link https://stage.codapayments.com/epcsimulator/

Step 4: Điền thông tin như hình sau:

-   Những thông tin cần nhập chính xác theo đơn hàng : MessageId điền id lấy từ
    URL như hình trên. Amount = Paid Amount = Amount hiển thị trên trang
    CODAPAY. Những thông tin khác nhập đúng như hình dưới.

![](media/2487addc89ee4ceb99fafc80782f8fa0.png)

Step 5: Click Submit

Step 6. Quay lại màn hình ở Step 2. Nhập Email rồi click continue \> Click
Close.

Step 7. WP trả về kết quả giao dịch thành công, users được nhận items.

**6b. Thanh Toán trên Production:**

Step 1: Chọn gói với phương thức thanh toán là Mobile Banking & Counter

Step 2: Click “Confirm”. WP sẽ điều hướng sang trang thanh toán CODAPAY.

Step 3: Nhập chính xác email \> Click continue

Step 4: User sẽ nhận được 1 email từ provider với nội dung hướng dẫn thanh toán.

Step 5: Hoàn tất thanh toán như hướng dẫn trong mail

Step 6: WP trả về kết quả giao dịch thành công, users được nhận items.

**7. Thanh toán qua PayPal**

\* Test trên Sandbox: Theo các steps bên dưới.

\* Test trên Production: Cần tạo tài khoản Paypal để thanh toán

7.1. Cách tạo tài khoản Paypal trên Production

-   Step 1: Đăng ký tại: <https://www.paypal.com/vn/welcome/signup>

-   Step 2: Liên kết với thẻ Visa/Master của cá nhân

-   Step 3: Mở trang WebPayment và thực hiện giao dịch, chọn thanh toán bằng tài
    khoản Paypal đã tạo và Thẻ Visa/Master vừa link.

Flow test: Login acc **BUY** trên webpay để thanh toán. Kiểm tra số tiền được
cộng thêm ở acc **BUSINESS** đúng với số tiền bỏ ra ở acc **BUY**

**BUY**: huynhminh009-buyer\@gmail.com – 12345679

**BUSINESS:** huynhminh009-business\@gmail.com – 12345679

**Sandbox Paypal**: <https://www.sandbox.paypal.com/us/home>

Step 1: Vào sandbox paypal login bằng **BUSINESS**

![](media/42415ae4c6f9f8961a031c2e5b15dfe0.png)

Step 2: Chọn Pack -\> Chọn thanh toán bằng paypal trên webpay

![](media/0ef0bbec4de3605409994f3f1ac50c3e.png)

Step 3: Login acc **BUY**

Step 4a: Kiểm tra giá tiền của pack

Step 4b: Chọn 1 trong 2 đều được

Step 4c: Chọn **Continue**

![](media/b59bf9b8009bcfeaa78861230fb61980.png)

Step 4d: Chọn **Pay Now**

![](media/9171039c93027f21f3cae2983477fdd0.png)

Step 5: Kiểm tra kết quả giao dịch

![](media/2bfcd030a74c857132de9c0db9966627.png)

Step 6: Chờ 20s refresh **step1**

![](media/351b40b5701ca9b181fba7a7f96fa444.png)

**=\> Hiển thị đúng số tiền của pack đã mua ở acc BUY**
