#### [Template] 1010-WXi-FRD-RDL-Rule Definition Detail

May 18, 2019 [Project
Requirement](https://wiki.mto.zing.vn/category/gt-automation-ga/project-requirement/),
[Project
Requirement_QC](https://wiki.mto.zing.vn/category/mto-billing/project-requirement_qc/)

1010-WXi-SDD-RDL-Rule Definition List
=====================================

IN PROGRESS

[1.
Overview](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

[2. Use Case
Specification](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.1. UC1: Search
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.1.1. Activity Flow
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.1.2.
>   Wireframe](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.1.3. Business
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.2. UC2: View Rule Definition
>   List](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.2.1. Activity Flow
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.2.2.
>   Wireframe](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.2.3. Business
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.3. UC3: Export Rule Definition
>   List](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.3.1. Activity Flow
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.3.2.
>   Wireframe](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.3.3. Business
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.4. UC4: Batch
>   Update](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.4.1. Activity Flow
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.4.2.
>   Wireframe](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.4.3. Business
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.5. UC5:
>   Print](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.5.1. Activity Flow
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.5.2.
>   Wireframe](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [2.5.3. Business
>   Rules](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

[3. Database
Design](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.1. ER
>   Diagram](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2. Detailed
>   Design](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.1. Table
>   “rule”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.2. Table
>   “rule_test_level”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.3. Table
>   “rule_previous_result”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.4. Table
>   “result_test”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.5. Table
>   “result_test_color”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.6. Table
>   “rule_comment”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.7. Table
>   “rule_site”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.8. Table
>   “rule_test_code”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.9. Table
>   “rule_profile_code”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.10. Table
>   “additional_rule”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [3.2.11. Table
>   “rule_type”](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

[4. API
Design](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.1. List all and search
>   items](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.2. Create new
>   item](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.3. Update an
>   item](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.4. Export
>   item](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.5. Batch update
>   items](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.6. Get one
>   item](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.7. Change
>   status](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.8. Get rule
>   type](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.9. Update rule
>   comment](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

>   [4.10. Delete rule
>   comment](https://wiki.mto.zing.vn/2019/05/18/template-1010-wxi-frd-rdl-rule-definition-detail/#id-1010-WXi-SDD-RDL-RuleDefinitionList-)

**1. Overview**
===============

This feature allows user to search, view list of created Rule definition, export
the result list to a specific file format (.xls or .csv), print PDF and Batch
Update for multiple rules.

**2. Use Case Specification**
=============================

This section covers the system’s functional requirements which details what the
system must do in terms of input, behavior and the expected output. It elicits
the interaction between the actor(s) and the system, the system’s behavior and
the results of their interactions.

**2.1. UC1: Search Rules**
--------------------------

| **Functional Requirement \# (FR-MSID-\#\#\#)** | FR-MSID-001                                                                                                                                                                                                                                                                                                                                                                                  |
|------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Associated Jira Story**                      | [WX-2478](https://jiraapp.sysmex.com/browse/WX-2478) – Getting issue details…STATUS                                                                                                                                                                                                                                                                                                          |
| **Actor:**                                     | · Service User · Customer User                                                                                                                                                                                                                                                                                                                                                               |
| **Trigger:**                                   | User selects the selection criteria to search from Rule Definition List Screen and clicks ‘Search’                                                                                                                                                                                                                                                                                           |
| **Pre-condition:**                             | · User logs in successfully to the system. · For Customer User, he/she must be within user group who has access to Rule Definition List · User clicks Dashboard \> System Configuration \> Rules \> Rule Definition. The Rule Definition screen will be displayed. · *On Rule Definition List screen, the Rules List must be within the site of the user who access to Rule Definition List* |
| **Post-condition:**                            | Search Results are returned correctly.                                                                                                                                                                                                                                                                                                                                                       |

### 2.1.1. Activity Flow Diagram

Software Design Requirement \#: SDR-MSID-001

### 2.1.2. Wireframe

Software Design Requirement \#: SDR-MSID-002

**Figure 1.** Search Rule screen

| **SDR\#**           | **Item Name**                                                          | **Field Type** | **Required (Y/N)** | **Description**                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|---------------------|------------------------------------------------------------------------|----------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| Selection Criteria  |                                                                        |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-003        | Rule Name                                                              | Textbox        | N                  | Allow user to input data to search rules by rule name Max length = 20 Auto suggestion when user enters character to field.                                                                                                                                                                 |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-004        | Rule Type                                                              | Dropdown List  | N                  | Default value = “-Select-” Value list: · WBC Rule · RBC Rule · PLT Rule · Additional Rule · Body Fluid Rule · Function Rule · A1C Rule · Sed Rate Rule · Order Creation · Critical Rules · Delta Rules Single selection Allow user to select the above values to search rules by rule type |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-005        | Additional Rule                                                        | Dropdown List  | N                  | Default value = “-Select-” Single Selection Value list will be populated based on selected Rule Type (Hardcoded):                                                                                                                                                                          |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| **Rule Type**       | **Additional Rule**                                                    |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| WBC Rule            | · WBC ABNORMAL · WBC LINEARITY · WBC PATH REVIEW · WBC SUSPECT IP FLAG |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| RBC Rule            | · RBC ABNORMAL · RBC LINEARITY · RBC PATH REVIEW · RBC SUSPECT IP FLAG |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| PLT Rule            | · PLT ABNORMAL · PLT LINEARITY · PLT PATH REVIEW · PLT SUSPECT IP FLAG |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Additional Rule     | · ADDITIONAL                                                           |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Body Fluid Rule     | · BF LINEARITY · BF ABNORMAL                                           |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Function Rule       | · ACTION MESSAGE · POSITIVE AND ERROR JUDGEMENT                        |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| A1C Rule            | · BIORAD                                                               |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Sed Rate Rule       | · ALIFAX · INTERRLINER                                                 |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Order Creation Rule | · PREFLEX                                                              |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Critical Rule       | · WBC CRITICAL · RBC CRITICAL · PLT CRITICAL · OTHER CRITICAL          |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| Delta Rule          | · RBC DELTA · PLT DELTA · OTHER DELTA                                  |                |                    |                                                                                                                                                                                                                                                                                            |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-006        | Status                                                                 | Dropdown List  | N                  | Default value = “-Select-” Single Selection Value list: · Active · Inactive Allow user to search rules by status                                                                                                                                                                           |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-007        | Apply                                                                  | Button         | N/A                | On click, system will search and return search results on the same screen                                                                                                                                                                                                                  |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| SDR-MSID-008        | Clear                                                                  | Button         | N/A                | On click, system will clear all search results and redirect to search default screen                                                                                                                                                                                                       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

Allow user to search rules by additional rule

Additional Rule field will be disabled until user selects “Rule Type”

### 2.1.3. Business Rules

| **SDR\#**    | **BR Code** | **Description**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|--------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SDR-MSID-009 | BR 1        | System lists all Rule records which match inputted search criteria from Selection Criteria section and display the Search Results in the data table with the following order: · Rule Name · Rule Type · Additional Rule · Active Month(s) · Description Sort column: Rule Name. Default Sort is Ascending (in Alphabet) Paging: By default: 20 items/page. User can select number of items/page to be displayed If system cannot find any matched results, prompt a notification message as MSG 1 “*No record found!”* |
