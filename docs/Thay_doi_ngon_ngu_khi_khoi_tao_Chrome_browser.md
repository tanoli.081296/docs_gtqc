#### Thay đổi ngôn ngữ khi khởi tạo Chrome browser

July 1, 2019 [Automation](https://wiki.mto.zing.vn/category/gt-qc/automation/)

Có rất nhiều bạn hỏi mình khi chạy một ví dụ với trang google. Thường thì ô tìm
kiếm có attribute là **name=q** nhưng đối với một số máy tính nó lại hiện thị là
**name=Tìm Kiếm** và đoạn code nó sẽ báo lỗi là ko tìm thấy đối tượng.

Bản chất của vấn đề này là do Chrome nó tự động nhận ra đây là Việt Nam nên nó
load content Tiếng Việt mặc định. để có thể load tiếng anh bạn có thể sử dụng
đoạn code dưới đây lúc khởi tạo ChromDriver:

HashMap\<String, Object\> chromePrefs = new HashMap\<String, Object\>();
chromePrefs.put(“intl.accept_languages”, “en”); ChromeOptions options = new
ChromeOptions(); options.setExperimentalOption(“prefs”, chromePrefs); driver =
new ChromeDriver(options);

Source:
https://www.testingvn.com/viewtopic.php?f=33&t=192350&fbclid=IwAR1Gm_3I6Z0uHc_AUyCgmXzRtYqdcMFmQsvknOO0CpjH46Y-oVTQKR4VmUo
