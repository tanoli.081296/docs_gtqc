# **Hướng dẫn thanh toán bằng phương thức Zalopay trên Sandbox**

\* Test trên Sandbox: Xem mục 2.1 dưới đây để tạo account Sandbox.

\* Test trên Production: Thanh toán qua app ZaloPay bằng account real của cá nhân.

*2.1. Cách tạo tài khoản ZaloPay Sandbox account trên Android devices: (khuyến khích dùng thiết bị Android để test)*

-   Bước 1: Sử dụng số điện thoại của cá nhân để tạo acc Zalo.

-   Bước 2: Cài Apk ZaloPay Sandbox bằng link sau: [ZaloPay Sandbox
    Apk](https://drive.google.com/file/d/1NefEyXHC8jD1VXKpydfPubLYRFWwHjVL/view)

-   Bước 3: Mở app Zalopay Sandbox lên và tạo liên kết với tài khoản Zalo.

-   Lưu ý:

    -   – **Nếu là máy cá nhân** nên đặt mật khẩu zalopay sandbox và real
        **giống nhau** để tránh báo lỗi mật khẩu không đúng.

    -   – Nếu vẫn báo sai vui lòng gửi **mã định danh** cho anh Nhựt domain: **NhutBM** để hỗ trợ reset mật khẩu.

![](media/8081a2aae737312ea533bbe40f198c56.png)

![](media/3dd7152d30ece506b1a4d0cd5e73c9f3.png)

-   Bước 4: Contact domain **NhutBM** để chuyển tiền
    cho số điện thoại vừa tạo acc ZaloPay Sandbox.

*2.2. Nạp qua Browsers trên PC/Laptop*

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán là ZaloPay

-   Bước 3: Click “Xác nhận”, trang web sẽ trả về QR Code

-   Bước 4: Sử dụng devices cài sẵn ZaloPay Sandbox, quét mã QR Code để xác
    nhận thanh toán

-   Bước 5: Web shop trả về kết quả Thanh toán thành công

-   Bước 6: User vào game và kiểm tra nhận được vật phẩm tương ứng

*2.3. Nạp qua Browsers của Mobile devices*

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán là ZaloPay

-   Bước 3: Click “Xác nhận” thanh toán, trang web sẽ điều hướng để mở ứng dụng ZaloPay Sandbox

-   Bước 4: Hiện lên popup yêu cầu mở ZaloPay Sandbox để tiến thành thanh toán

-   Bước 5: Mở ZaloPay Sandbox xác nhận thanh toán