#### Hướng dẫn sử dụng GT OVPN để addhost phục vụ việc test trên Mobile

May 14, 2019 [How to
test](https://wiki.mto.zing.vn/category/mto-billing/testing/), [Project
Requirement_QC](https://wiki.mto.zing.vn/category/mto-billing/project-requirement_qc/)

\*\*\***Download .ovpn config files to use**: <https://ovpn.mto.zing.vn/>
=========================================================================

![](media/image1.png)

### **I. Device Android**

### 1. Step 1: Mở Google Play tìm app Open VPN và tải về

![](media/image2.png)

### 2. Step 2: Mở app OpenVPN chọn ”OVPN Profile”

![](media/image3.png)

### 3. Step 3: Chọn file ovpn đã tải về trước đó \> Import

![](media/image4.png)

### 4. Step 4: Chọn Add (có thể thay đổi title cho dễ nhớ)

###  =\> Nhập username/password ở link down

![](media/image5.png)

### 5. Step 5: Enable để sử dụng

![](media/image6.png)

![](media/image7.png)

**II. Device IOS**
------------------

### Bước 1: Mở Appstore search app OpenVPN và tải về

![](media/image8.png)

### Bước 2: Mở file Ovpn đã tải trước đó \> Chọn “Sao chép đến OpenVPN”

![](media/image9.png)

### Bước 3: Chọn Add

![](media/image10.png)

### Bước 4: Chọn Add(có thể thay Title cho dễ nhớ)

###  =\> Nhập username/password ở link down file vpn

![](media/2d524c3583a4fe32f6449e8df04d4596.png)

### Bước 5: Next các bước xác thực trên điện thoại

### Bước 6: Enable để sử dụng

![](media/image12.png)

**\*\*\*Kiểm tra đã addhost thành công:**
-----------------------------------------

### Truy cập [http://checkvpn.framework.gt.vng.vn](http://checkvpn.framework.gt.vng.vn/) \> Hiển thị như hình là addhost thành công

![](media/image13.png)
