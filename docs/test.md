
# [VN] Hướng dẫn thanh toán trên Sandbox ở thị trường VN


## 
## **I. Login:**

-   Bước 1: Chọn product/game cần test trên Sandbox (ex:
<https://sandbox-pay.mto.zing.vn/wplogin/mobile/gamecode>)

-   Bước 2: Click vào icon tương ứng để chọn phương thức login của mỗi product
(Zalo/Facebook/Chơi Ngay/Google/Nạp Nhanh…)

![](media/70e6x1004ead9f4c0eec81c538701c8a1.png)

-   Bước 3: Sau khi Login thành công, web shop trả về thông tin user dựa theo dữ liệu
mà products cung cấp (username, avatar, level, etc…), xác nhận đúng tài khoản
trước khi tiến hành thanh toán ở bước kế tiếp

![](media/d5a9d38a46b250c1cbe3d8d26de177ee.png)

## **II. Tiến hành thanh toán:**

*1. Với product sử dụng gói vật phẩm:*

-   Bước 1: Chọn gói vật phẩm cần mua trong danh sách hiện có

![](media/huyvh1.png)

-   Bước 2: Xác nhận "Chọn gói nạp này"

-   Bước 3: Chọn phương thức thanh toán tương ứng sẽ hiện ra với mỗi gói

	| Phương thức thanh toán 	| Mệnh giá thanh toán (VNĐ)              	|
	|------------------------	|----------------------------------------	|
	| ZingCard               	| 10K, 20K, 50K, 100K, 200K, 500K, 1000K 	|
	| ATM/iBanking           	| Tất cả các mệnh giá                    	|
	| Thẻ tín dụng           	| Tất cả các mệnh giá                    	|
	| Zalopay                	| Tất cả các mệnh giá                    	|
	| SMS VNPT               	| 10K, 20K, 50K, 100K, 200K              	|
	| SMS Viettel            	| 10K, 20K, 50K, 100K, 200K              	|

    -   Lưu ý mệnh giá quy đổi SMS (Áp dụng theo chính sách tỉ giá VNG)

        | Giá tiền | 10K     | 20K      | 50K      | 100K     | 200K      |
        |----------|---------|----------|----------|----------|-----------|
        | Point    | 8 point | 16 point | 40 point | 80 point | 160 point |

    -	Phương thức SMS chỉ hỗ trợ thanh toán tối đa gói giá trị 200.000 VNĐ.

    -   Phương thức Ví tồn chỉ hỗ trợ thanh toán gói có **giá trị nhỏ hơn hoặc
        bằng** số point hiện có ở ví tồn.

*2. Với product không sử dụng gói vật phẩm*: 
-   Hỗ trợ cả 4 phương thức thanh toán: 
      - Nạp ZaloPay, 
      - Nạp ZingCard
      - Nạp ATM/iBanking 
      - Nạp thẻ tín dụng
      - Nạp SMS

## **III. Các phương thức thanh toán:**

# **1. Nạp thẻ Zing Card**

\* Test trên Sandbox: Xem mục 1.1 dưới dây để lấy thẻ Sandbox.

\* Test trên Production: Mua trực tiếp trên OA Zalo: *Esale Hệ thống bán lẻ trực tuyến* hoặc <https://esale.zing.vn/>

1.1. Cách lấy thẻ Zing Card trên Sandbox:

-   Bước 1: Truy cập link: <https://sandbox.mresource.pay.zing.vn/card/zing>

-   Bước 2: Login bằng domain VNG.

-   Bước 3: Chọn mệnh giá thẻ cần generate.

-   Bước 4: Sử dụng Serial/PIN code được cung cấp để thanh toán trên Sandbox.

\*\*\*Nếu không vào được link gen card thì liên hệ domain anh Chiêu: **ChieuVH** để được giúp đỡ.

**Hướng dẫn test nạp thẻ zing card trên sandbox**

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán ZingCard

-   Bước 3: Điền thông tin thẻ (serial, mã nạp) đã được generate từ bước trên

-   Bước 4: Click button “Xác nhận” để nạp tiền

![](media/babf82b1d956cc412d1b7efb40161aec.png)

Bước 5a: Nếu thẻ hợp lệ: Sẽ trả ra trang kết quả giao dịch Thanh toán
thành công (users được cộng items)

![](media/2599536e943e99c9d8a78f97282dfb98.png)

Bước 5b: Nếu thẻ không hợp lệ: Sẽ hiển thị ngay thông báo lỗi ở ô Nạp thẻ
cào (users không được cộng items)

![](media/4e3a1b4b1127aab4fa2e533050193a79.png)

*Nếu game có bật filter mệnh giá thẻ theo giá gói khi nhập số seri sẽ hiển thị lỗi

![](media/huyvh2.png)

# **2. Nạp bằng Zalopay qua tài khoản ZaloPay sandbox**

\* Test trên Sandbox: Xem mục 2.1 dưới đây để tạo account Sandbox.

\* Test trên Production: Thanh toán qua app ZaloPay bằng account real của cá nhân.

*2.1. Cách tạo tài khoản ZaloPay Sandbox account trên Android devices: (khuyến khích dùng thiết bị Android để test)*

-   Bước 1: Sử dụng số điện thoại của cá nhân để tạo acc Zalo.

-   Bước 2: Cài Apk ZaloPay Sandbox bằng link sau: [ZaloPay Sandbox
    Apk](https://drive.google.com/file/d/1NefEyXHC8jD1VXKpydfPubLYRFWwHjVL/view)

-   Bước 3: Mở app Zalopay Sandbox lên và tạo liên kết với tài khoản Zalo.

-   Lưu ý:

    -   – **Nếu là máy cá nhân** nên đặt mật khẩu zalopay sandbox và real
        **giống nhau** để tránh báo lỗi mật khẩu không đúng.

    -   – Nếu vẫn báo sai vui lòng gửi **mã định danh** cho anh Nhựt domain: **NhutBM** để hỗ trợ reset mật khẩu.

![](media/8081a2aae737312ea533bbe40f198c56.png)

![](media/3dd7152d30ece506b1a4d0cd5e73c9f3.png)

-   Bước 4: Contact domain **NhutBM** để chuyển tiền
    cho số điện thoại vừa tạo acc ZaloPay Sandbox.

*2.2. Nạp qua Browsers trên PC/Laptop*

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán là ZaloPay

-   Bước 3: Click “Xác nhận”, trang web sẽ trả về QR Code

-   Bước 4: Sử dụng devices cài sẵn ZaloPay Sandbox, quét mã QR Code để xác
    nhận thanh toán

-   Bước 5: Web shop trả về kết quả Thanh toán thành công

-   Bước 6: User vào game và kiểm tra nhận được vật phẩm tương ứng

*2.3. Nạp qua Browsers của Mobile devices*

-   Bước 1: Truy cập link: <https://sandbox-pay.mto.zing.vn/> và chọn sản phẩm cần test

-   Bước 2: Chọn phương thức thanh toán là ZaloPay

-   Bước 3: Click “Xác nhận” thanh toán, trang web sẽ điều hướng để mở ứng dụng ZaloPay Sandbox

-   Bước 4: Hiện lên popup yêu cầu mở ZaloPay Sandbox để tiến thành thanh toán

-   Bước 5: Mở ZaloPay Sandbox xác nhận thanh toán

# **3. Nạp ATM/iBanking**

\* Test trên Sandbox: **chỉ test được flow tới bước hiển thị form nhập thông
tin.**

\* Test trên Production: Dùng tài khoản hoặc thẻ ATM có iBanking để thanh toán.

# **4. Nạp thẻ tín dụng**

\* Test trên Sandbox:

-   Bước 1: Nhập thông tin thẻ

\+ Số thẻ: **4111 1111 1111 1111**

\+ Tên, ngày hết hạn, CVV: **tùy ý**

-   Bước 2: Chọn Thanh toán

![](media/feeb29c94e21747167813f16485dd71a.png)

![](media/cb3bba1b7c0bc7d49566640f56e9d92e.png)

![](media/e9b2da78c94da7317eea85cbe03c391b.png)

![](media/huyvh3.png)

\* Test trên Production: Dùng thẻ Credit Card (Visa, Master, JCB) để thanh toán.

# **5. Nạp SMS trên Sandbox trên Windows**

\* Test trên Production: Dùng sim trả trước của cá nhân để thanh toán.

\* Test trên Sandbox: *Làm theo các bước dưới đây*

-   Bước 1.  Mở notepad bằng **Run as administrator**

-   Bước 2.  Chọn theo đường dẫn: **C:\\Windows\\System32\\drivers\\etc\\hosts**

-   Bước 3.  Addhost**: 10.30.8.220 sandbox.mresource.pay.zing.vn** -\> **Save**

![](media/24cb84989c22e4392c1cae88d731426d.png)

![](media/96021b94b3bc608da48f9570a4640369.png)

**Link test**

-   Viettel:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/viettel-charging-gateway/>

-   Vinaphone:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/vnpt-charging-gateway/>

**Flow test:**

*1.  Cú pháp 1: DK*

-   Bước 1: Copy cú pháp  
    

   ![](media/148d3d04b8e1d8a18aebc8014fcc8268.png)

      
-   Bước 2: Paste cú pháp, chọn config theo hình. Thay đổi syntax (dán cú pháp), amount (giá tiền phù hợp), Msisdn (nhập tùy ý), Mode (chọn real).

>   **Viettel**   
>    

![](media/ecd2cfa9f3f870f4ca1426c00b0c7765.png)

>   **Vinaphone**  
>   

![](media/bc269a3ef1598fda74fe8c3314f84ce0.png)

     
-   Bước 3: Chọn **SubmitOrder** -\> “Result: 0\|Thanh cong (VNG DK10 KTM 20190710000005822)”  
-   Bước 4: Kiểm tra kết quả giao dịch ở trang payment

*2.  Cú pháp 2: NAP*  

-   Bước 1: Copy cú pháp  
    

    ![](media/04cc04e913c2bd4bf6e5663621b8533c.png)

      
-   Bước 2: Paste cú pháp, chọn config theo hình. Thay đổi syntax (dán cú pháp), amount (giá tiền phù hợp), Msisdn (nhập tùy ý), Mode (chọn real).
    

    ![](media/1770b11ceb7b5abd2d22733c06a372b8.png)

      
-   Bước 3: Chọn **SubmitOrder** -\>”Result: 0\|Ban da nap thanh cong 10000 VND vao game Gun
    Pow. Chi tiet LH https://www.facebook.com/gunpow.360game.vn/”  
-   Bước 4: Kiểm tra kết quả giao dịch ở trang payment
