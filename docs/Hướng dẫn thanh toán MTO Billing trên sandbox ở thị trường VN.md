# [VN] Hướng dẫn thanh toán trên Sandbox ở thị trường VN

Hướng dẫn mọi người cách tiến hành login và thanh toán Web Payment trên môi
trường Sandbox.

## 
## **Step I. Login:**

1. Chọn product/game cần test trên Sandbox (ex:
<https://sandbox-pay.mto.zing.vn/wplogin/mobile/gamecode>)

2. Click vào icon tương ứng để chọn phương thức login của mỗi product
(Zalo/Facebook/Chơi Ngay/Google/Nạp Nhanh…)

![](media/70e6x1004ead9f4c0eec81c538701c8a1.png)

3. Sau khi Login thành công, web payment trả về thông tin user dựa theo dữ liệu
mà products cung cấp (username, avatar, level, etc…), xác nhận đúng tài khoản
trước khi tiến hành thanh toán ở bước kế tiếp

![](media/d5a9d38a46b250c1cbe3d8d26de177ee.png)

## **Step II. Tiến hành thanh toán:**

*1. Với product sử dụng gói vật phẩm:*

-   Step II.a. Chọn gói vật phẩm cần mua trong danh sách hiện có

![](media/43280d1d117d7bbc0a9e868d4e533764.png)

-   Step II.b: Xác nhận Chọn Gói Nạp Này

-   Step II.c: Chọn phương thức thanh toán tương ứng sẽ hiện ra với mỗi gói

    -   Gói giá trị thấp hơn 50.000 VNĐ: Hỗ trợ 2 phương thức thanh toán (Nạp
        ZaloPay, Nạp ZingCard).

    -   Gói giá trị từ 200.000 VNĐ đến 1.000.000 VNĐ: Hỗ trợ cả 4 phương thức
        thanh toán (Nạp ZaloPay, Nạp ZingCard, Nạp ATM/iBanking và Nạp thẻ tín
        dụng).

    -   Gói giá trị lớn hơn 1.000.000 VNĐ: Hỗ trợ 3 phương thức thanh toán (Nạp
        ZaloPay, Nạp ATM/iBanking và Nạp thẻ tín dụng).

    -   Phương thức SMS chỉ hỗ trợ thanh toán tối đa gói giá trị 200.000 VNĐ.

    -   Phương thức Ví tồn chỉ hỗ trợ thanh toán gói có **giá trị nhỏ hơn hoặc
        bằng** số point hiện có ở ví tồn.

    -   \*\*\*Lưu ý mệnh giá quy đổi SMS (Áp dụng ở 1 số game)

        -   **10k ⇔ 8 Point**

        -   **20k ⇔ 16 Point**

        -   **50k ⇔ 40 Point**

        -   **100k ⇔ 80 Point**

        -   **200k ⇔ 160 Point**

*2. Với product không sử dụng gói vật phẩm*: Hỗ trợ cả 4 phương thức thanh toán
(Nạp ZaloPay, Nạp ZingCard, Nạp ATM/iBanking, Nạp thẻ tín dụng, Nạp SMS)

## **III. Các phương thức thanh toán:**

1. Nạp thẻ Pre-paid Card

\* Test trên Sandbox: Xem mục 1.1 dưới dây để lấy thẻ Sandbox.

\* Test trên Production: Mua trực tiếp trên app ZaloPay.

1.1. Cách lấy thẻ Zing Card trên Sandbox:

-   Step 1: Truy cập link: <https://sandbox.mresource.pay.zing.vn/card/zing>

-   Step 2: Login bằng domain VNG.

-   Step 3: Chọn mệnh giá thẻ cần generate.

-   Step 4: Sử dụng Serial/PIN code được cung cấp để thanh toán trên Sandbox.

\*\*\*Nếu không vào được link gen card thì add host

#### 10.30.8.220 sandbox.zingcardv2new.vng.com.vn zingcard.pay.zing.vn sandbox.mresource.pay.zing.vn

Chưa biết cách add có thể xem thêm ở bài viết
<https://wiki.mto.zing.vn/2019/05/14/huong-dan-su-dung-ovpn-de-addhost-phuc-vu-viec-test-tren-pc/>

\*\*\*Nếu làm các bước trên mà vẫn không vào được thì liên hệ anh
**Chieuvh\@vng.com.vn** để được giúp đỡ.

*\* Trên Sandbox hiện mới chỉ có ZingCard.*

Step III.1a: Chọn phương thức thanh toán ZingCard

Step III.1b: Điền thông tin thẻ (serial, mã nạp)

Step III.1c: Click button “Xác nhận” để nạp tiền

![](media/babf82b1d956cc412d1b7efb40161aec.png)

Step III.1d-1: Nếu thẻ hợp lệ: Sẽ trả ra trang kết quả giao dịch Thanh toán
thành công (users được cộng items)

![](media/2599536e943e99c9d8a78f97282dfb98.png)

Step III.1d-2: Nếu thẻ không hợp lệ: Sẽ hiển thị ngay thông báo lỗi ở ô Nạp thẻ
cào (users không được cộng items)

![](media/4e3a1b4b1127aab4fa2e533050193a79.png)

2. Nạp ZaloPay qua tài khoản ZaloPay sandbox

\* Test trên Sandbox: Xem mục 2.1 dưới đây để tạo acc Sandbox.

\* Test trên Production: Thanh toán qua app ZaloPay bằng acc của cá nhân.

2.1. Cách tạo tài khoản ZaloPay Sandbox account trên Android devices:

-   Step 1: Sử dụng số điện thoại của cá nhân để tạo acc Zalo.

-   Step 2: Cài Apk ZaloPay Sandbox bằng link sau: [ZaloPay Sandbox
    Apk](https://drive.google.com/file/d/1NefEyXHC8jD1VXKpydfPubLYRFWwHjVL/view)

-   Step 3: Mở app Zalopay Sandbox lên và tạo acc liên kết với tài khoản Zalo.

-   Lưu ý:

    -   – **Nếu là máy cá nhân** nên đặt mật khẩu zalopay sandbox và real
        **giống nhau** để tránh báo lỗi mật khẩu không đúng.

    -   – Nếu vẫn báo sai vui lòng gửi **mã định danh** cho manhtt\@vng.com.vn
        hoặc nhutbm\@vng.com.vn để hỗ trợ reset mật khẩu.

![](media/8081a2aae737312ea533bbe40f198c56.png)

![](media/3dd7152d30ece506b1a4d0cd5e73c9f3.png)

-   Step 4: Contact manhtt\@vng.com.vn hoặc nhutbm\@vng.com.vn để chuyển tiền
    cho số điện thoại vừa tạo acc ZaloPay Sandbox.

2.2. Nạp qua Browsers trên PC/Laptop

-   Step 2a-1: Chọn phương thức thanh toán là ZaloPay

-   Step 2a-2: Click “Xác nhận”, trang web sẽ trả về QR Code

-   Step 2a-3: Sử dụng devices cài sẵn ZaloPay Sandbox, quét mã QR Code để xác
    nhận thanh toán

-   Step 2a-4: web payment trả về kết quả Thanh toán thành công

-   Step 2a-5: User vô game nhận items

2.3. Nạp qua Browsers của Mobile devices

-   Step III.2b-1: Chọn phương thức thanh toán là ZaloPay

-   Step III.2b-2: Click “Xác nhận” thanh toán, trang web sẽ điều hướng để mở
    ứng dụng ZaloPay

-   Step III.2b-3: Hiện lên popup yêu cầu mở ZaloPay để tiến thành thanh toán

-   Step III.2b-4: Mở ZaloPay xác nhận thanh toán

3. Nạp ATM/iBanking

\* Test trên Sandbox: **chỉ test được flow tới bước hiển thị form nhập thông
tin.**

\* Test trên Production: Dùng tài khoản hoặc thẻ ATM có iBanking để thanh toán.

4. Nạp thẻ tín dụng

\* Test trên Sandbox:

Step 1: Nhập thông tin thẻ

\+ Số thẻ: **4111 1111 1111 1111**

\+ Tên, ngày hết hạn, CVV: **tùy ý**

Step 2: Chọn Thanh toán

![](media/feeb29c94e21747167813f16485dd71a.png)

![](media/cb3bba1b7c0bc7d49566640f56e9d92e.png)

![](media/e9b2da78c94da7317eea85cbe03c391b.png)

\* Test trên Production: Dùng thẻ Credit Card (Visa, Master, JCB) để thanh toán.

5. Nạp SMS trên Sandbox trên Windows

\* Test trên Production: Dùng sim trả trước của cá nhân để thanh toán.

\* Test trên Sandbox: *Làm theo các bước dưới đây*

1.  Mở notepad bằng **Run as administrator**

2.  Chọn theo đường dẫn: **C:\\Windows\\System32\\drivers\\etc\\hosts**

3.  Addhost**: 10.30.8.220 sandbox.mresource.pay.zing.vn** -\> **Save**

![](media/24cb84989c22e4392c1cae88d731426d.png)

![](media/96021b94b3bc608da48f9570a4640369.png)

**Link test**

-   Viettel:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/viettel-charging-gateway/>

-   Vinaphone:
    <https://sandbox.mresource.pay.zing.vn/mvas9029/pub/vnpt-charging-gateway/>

Flow test:

1.  Cú pháp: DK  
    1.1 Copy cú pháp  
    

    ![](media/148d3d04b8e1d8a18aebc8014fcc8268.png)

      
    1.2 Paste cú pháp, chọn config theo hình  
    Thay đổi **syntax, amount** phù hợp với từng mệnh giá  
    **Viettel**  
    

    ![](media/ecd2cfa9f3f870f4ca1426c00b0c7765.png)

>   **Vinaphone**  
>   

![](media/bc269a3ef1598fda74fe8c3314f84ce0.png)

>     
>   1.3 SubmitOrder -\> “Result: 0\|Thanh cong (VNG DK10 KTM 20190710000005822)”  
>   1.4 Kiểm tra kết quả giao dịch ở trang payment

1.  Cú pháp: NAP  
    2.1 Copy cú pháp  
    

    ![](media/04cc04e913c2bd4bf6e5663621b8533c.png)

      
    2.2 Paste cú pháp, chọn config theo hình  
    Thay đổi syntax, amount phù hợp với từng mệnh giá  
    

    ![](media/1770b11ceb7b5abd2d22733c06a372b8.png)

      
    2.3 SubmitOrder -\>”Result: 0\|Ban da nap thanh cong 10000 VND vao game Gun
    Pow. Chi tiet LH https://www.facebook.com/gunpow.360game.vn/”  
    2.4 Kiểm tra kết quả giao dịch ở trang payment
