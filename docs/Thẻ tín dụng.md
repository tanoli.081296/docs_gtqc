# **Hướng dẫn thanh toán WebShop bằng phương thức Thẻ tín dụng trên Sandbox**

\* Test trên Sandbox:

-   Bước 1: Nhập thông tin thẻ

\+ Số thẻ: **4111 1111 1111 1111**

\+ Tên, ngày hết hạn, CVV: **tùy ý**

-   Bước 2: Chọn Thanh toán

![](media/feeb29c94e21747167813f16485dd71a.png)

![](media/cb3bba1b7c0bc7d49566640f56e9d92e.png)

![](media/e9b2da78c94da7317eea85cbe03c391b.png)

![](media/huyvh3.png)

\* Test trên Production: Dùng thẻ Credit Card (Visa, Master, JCB) để thanh toán.